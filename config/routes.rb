ActionController::Routing::Routes.draw do |map|
  # Add your own custom routes here.
  # The priority is based upon order of creation: first created -> highest priority.
  
  # Here's a sample route:
  # map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # You can have the root of your site routed by hooking up '' 
  # -- just remember to delete public/index.html.
  # map.connect '', :controller => "welcome"

  # Allow downloading Web Service WSDL as a file with an extension
  # instead of a file named 'wsdl'
  #map.connect ':controller/service.wsdl', :action => 'wsdl'
 	
 	
  # routes admin controller requests
  #map.connect 'config/:action/:id', :controller => 'config'
  
  # routes account controller requests
  #map.connect 'account/:action/:id', :controller => 'account'
  
  # routes reports controller requests
  #map.connect 'reports/:action/:id', :controller => 'reports'
  
  # Install the default route
  map.connect ':controller/:action/:id/'
  
  # routes frontend requests to the root path
  map.connect ':action/:id', :controller => 'frontend'
 

 
end
