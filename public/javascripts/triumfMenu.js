jQuery.noConflict()(function($){

        $( document ).ready(function() {
           $('offcanvas').offcanvas({
                 canvas: ['main','.toggle-canvas']
              });


              if ($(window).width() > 750){  
                  $('offcanvas').offcanvas('open');
                  $( ".menustuff" ).css("left","-100000000000px");
                  $( 'offcanvas ul > li > a' ).css("font-size","");
                  $( 'offcanvas ul > li > a' ).css(
                    "line-height","");
              };
              if ($(window).width() < 750){  
                  $('offcanvas').offcanvas('close');
                  $( ".menustuff" ).css("left","");

                  $( 'offcanvas ul > li > a' ).css(
                    "font-size","25px");

                  $( 'offcanvas ul > li > a' ).css(
                    "line-height","30px");
              };


               $(window).resize(function(){
                  if ($(window).width() > 750){  
                    $('offcanvas').offcanvas('open');
                    $( ".menustuff" ).css("left","-100000000000px");
                  };
                  if ($(window).width() < 750){  
                    $('offcanvas').offcanvas('close');
                    $( ".menustuff" ).css("left","");
                    $( 'offcanvas ul > li > a' ).css("font-size","");
                  };
                });
        });

    });