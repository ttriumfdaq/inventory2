function checkAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = true ;
}

function uncheckAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = false ;
}

function open_tab(tab_name)
{
  //gets a handle on the tab
  var tab = $$$$(tab_name)
  
  //save whether or not tab_name is already open
  var tab_already_open = Element.hasClassName(tab_name, 'current')
  
	//close all tabs
	tabs = tab.parentNode.getElementsByTagName('li');
	for(j = 0; j < tabs.length; j++) {
	  Element.removeClassName(tabs[j], 'current')
	}
	
	//close all tab_content divs
	content_divs = document.getElementsByClassName('tab_content');
	for(j = 0; j < content_divs.length; j++) {
	  content_divs[j].style.display = 'none';
	}

  //if tab is not yet open then open it.
  if (tab_already_open == false)
  {
    Element.addClassName(tab_name, 'current')
    Element.show(tab_name + '_content')
  }
}

//concatenates the serialized output of the two sortable lists
//into one long post string (deals with the cases where one or
//more of the lists are empty)
function serialize_sortables()
{
  var hidden_data = Sortable.serialize('hidden');
  var displayed_data = Sortable.serialize('displayed');
  var output = '';
  if (hidden_data == '' && displayed_data == '')
  {
    return '';
  }
  else if (hidden_data != '' && displayed_data == '')
  {
    return hidden_data;
  }
  else if (hidden_data == '' && displayed_data != '')
  {
    return displayed_data;
  }
  else
  {
    return hidden_data + '&' + displayed_data;
  }
}  

//closes the show menu
function close_history()
{
  Element.update('history','');
  Element.hide('history_collapser');
  Element.show('history_launcher');
}  

//updates <title></title>
function update_page_title(title_text)
{
  document.title = title_text;
}  
