@@prefix = "/home/sclaret/inventory2/db/"


reader = CSV.open(@@prefix+'ncd.csv', 'r')
row0 = reader.shift
reader.close
@@cols_to_attrs_array = Array.new
row0.each_index {|index| @@cols_to_attrs_array[index] = row0[index] }


def saveitem(item_attributes,kind_name,category_id)
  item = Item.new
  item_attributes.keys.each{|key| item.send(key.to_s + '=', item_attributes[key]) }
  Item.setkind(kind_name,item)
  	Item.setcategory(category_id,item)
  item.updated_on = Time.now
  	item.created_on = Time.now
  if item.save
  	STDERR.puts "      added: " + kind_name
  else
  	STDERR.puts " !!!!!!!!!!failed!!!!!!!!!!"
  end
end

$overallcounter = 0

def processfile(filename,category)
	filename = @@prefix + filename
	STDERR.puts 'starting processing file ' + filename
	i=0
	i2=0
	for row in CSV.open(filename, 'r') 
	STDERR.puts row.join(' - ')
		if row[0]
			#skip header line
			if i!= 0
			    
			    
			    item_attributes = Hash.new
			    
			    @@cols_to_attrs_array.each_index{ |i| (item_attributes.store(@@cols_to_attrs_array[i], row[i])) unless @@cols_to_attrs_array[i] == 'name' }

				saveitem(item_attributes,row[0],category)

				i2 += 1
				$overallcounter += 1
			end
			i += 1
		end
	end
	STDERR.puts 'done processing file ' + filename + ', ' + (i2).to_s + ' items added to the database' 
end

processfile('1_1.csv','13')
processfile('1_3.csv','14')
processfile('1_4.csv','14')
processfile('1_7.csv','15')
processfile('1_8.csv','16')
processfile('1_9.csv','17')
processfile('1_10.csv','18')
processfile('1_11.csv','106')
processfile('2_1.csv','32')
processfile('2_2.csv','21')
processfile('2_3.csv','12')
processfile('2_4.csv','5')
processfile('2_6.csv','22')
processfile('2_7.csv','26')
processfile('2_8.csv','24')
processfile('3_1.csv','26')
processfile('3_2.csv','33')
processfile('3_6.csv','26')
processfile('5.csv','25')
processfile('6.csv','25')
processfile('7_1.csv','19')
processfile('7_2.csv','19')
processfile('8_2.csv','34')
processfile('10.csv','35')
processfile('camacandvmeinterfaces.csv','107')
processfile('mvmeandvmiccpus.csv','109')
processfile('emulex.csv','108')
processfile('tapedrives.csv','111')
processfile('vax.csv','112')
processfile('vmecrates.csv','113')
processfile('ncd.csv','110')

STDERR.puts $overallcounter.to_s + " items added overall."



@@cols_to_attrs_array.each_index{|i| STDERR.puts "#{i} : #{@@cols_to_attrs_array[i]}"}


#params = {"category"=>{"id"=>"106"}, "kind"=>{"name"=>"bobble"}, "item"=>{"barcode"=> '123456', 'po_number' => 'blablablob'}}
#saveitem(params)


