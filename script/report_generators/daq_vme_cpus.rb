module CustomReport

  #dataset mixes in the Ruby Enumerable module  so the following functions
  #are available to you (http://corelib.rubyonrails.org/classes/Enumerable.html): 
  #
  #all? any? collect detect each_cons each_slice each_with_index entries   
  #enum_cons enum_slice enum_with_index find find_all grep include? inject   
  #map max member? min partition reject select sort sort_by to_a to_set zip  
  #
  #dataset also adds in the following custom functions for you to use
  #(http://ruport.rubyforge.org/docs/classes/Ruport/DataSet.html):
  #
  #<< == [] []= as clone each empty? eql? load new remove_columns remove_columns!   
  #select_columns to_csv to_html to_s  
  def self.create_data_set
  
   	items = Item.find_all_by_category_id(109, :include => :kind).collect{|e| e.kind.attributes.merge!(e.attributes) }
    dataset = items.to_ds(items[0].keys)

    #throw out all columns except category_id and current_location
    return dataset.select_columns('name', 'current_location', 'machine_name', 'mac')
  end
	
end