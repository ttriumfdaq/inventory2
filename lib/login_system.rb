require_dependency "user"

module LoginSystem 
  protected
  def login_required
    if @session[:user]
      return true
    end
    access_denied
    return false 
  end
  #custom root login filter
  def root_login_required
    if (@session[:user] and @session[:user].has_root == true) || (@session[:user] and @session[:user].user_name == 'root')
      return true
    end
    access_denied(true)
    return false 
  end
  def access_denied(root_required = nil)
    session[:redirect_where_after_login] = request.request_uri
    redirect_to :controller=>"/account", :action =>"login", :root_required => root_required
  end  
end