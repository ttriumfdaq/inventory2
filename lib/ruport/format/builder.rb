module Ruport
  class Format
    class Builder
      
      def initialize( data_set )
        @data = data_set
        @original = data_set.dup
        @format = nil
        @range = nil
        @header = nil
        @footer = nil
        @output_type = nil
      end
      
      attr_accessor :format, :range, :header, :footer, :output_type
      
      def render
        @data = @range ? ( @original[@range] ) : ( @original )
        send("render_#{@format}")    
      end
      
      def render_csv
        csv_klass = defined?(FasterCSV) ? FasterCSV : CSV
        fields = @original.fields
        ( @header ? "#{@header}\n\n" : "" ) +
        @data.inject(csv_klass.generate_line(fields).chomp + "\n" ) { |out,r|  
          out <<  csv_klass.generate_line(fields.map { |f| r[f] }).chomp + "\n"
        } + ( @footer ? "\n#{@footer}\n" : "" )
      end
      
      def render_html
        head_text = ( if @header
            "  <tr>\n    <th colspan=#{@original.fields.length}>#{@header}" +
            "</th>\n  </tr>\n" 
        end )
        out = "<table>\n#{head_text}  <tr>\n    <th>"+
              "#{@original.fields.join('</th><th>')}" +
              "</th>\n  </tr>\n"
        @data.inject(out) do |html,row|
          html << row.inject("  <tr>\n    "){ |row_html, field|
                    row_html << "<td>#{field}</td>"
                  } + "\n  </tr>\n"
        end
        foot_text = ( if @footer 
            "  <tr>\n    <th colspan=#{@original.fields.length}>#{@footer}" +
            "</th>\n  </tr>\n" 
        end )
        
        out << "#{foot_text}</table>"   
        return out unless @output_type.eql?(:complete) 
        "<html><head><title></title></head><body>\n#{out}</body></html>\n"
      end

      def render_text
        header = @header ? "#{@header}\n" : ""
        header << "fields: ( #{ @original.fields.join(', ') } )\n"
        indices = (@range)  ? @range.to_a : (0...@original.to_a.length).to_a  
        @data.inject(header) do |output,row|
          output << "row#{indices.shift}: ( #{row.to_a.join(', ')} )\n"
        end + (@footer ? "#{@footer}\n" : "" )
        
      end
      def render_pdf
        
        return unless defined? PDF::Writer
        pdf = PDF::Writer.new
        pdf.margins_cm(0)
        @data.each do |page|
          unless page.eql?(@data.pages.first)
            pdf.start_new_page
          end
          page.each do |section|
            section.each do |element|
              pdf.y =  pdf.cm2pts(element.top)
              pdf.text element.content, 
                          :left  => pdf.cm2pts(element.left),
                          :right => pdf.cm2pts(element.right),
                          :justification => element.align || :center
                                         
            end
          end
        end
        pdf.render

      end
    end
  end
end

