# --
# data_row.rb : Ruby Reports row abstraction
#
# Author: Gregory T. Brown (gregory.t.brown at gmail dot com)
#
# Copyright (c) 2006, All Rights Reserved.
#
# This is free software.  You may modify and redistribute this freely under
# your choice of the GNU General Public License or the Ruby License. 
#
# See LICENSE and COPYING for details
# ++
module Ruport 
    
  # DataRows are Enumerable lists which can be accessed by field name or ordinal
  # position.  
  #
  # They feature a tagging system, allowing them to be easily
  # compared or recalled.  
  #
  # DataRows form the elements of DataSets
  #
    class DataRow 
  
      include Enumerable
    
      # Takes data and field names as well as some optional parameters and
      # constructs a DataRow.  
      # 
      #
      # <tt>data</tt> can be specified in Hash, Array, or DataRow form
      #
      # Options: 
      # <tt>:filler</tt>::  this will be used as a default value for empty
      # <tt>:tags</tt>::    an initial set of tags for the row
      # 
      #
      # Examples:
      #   >> Ruport::DataRow.new [1,2,3,4,5], [:a,:b,:c,:d,:e], 
      #      :tags => %w[cat dog]
      #   => #<Ruport::DataRow:0xb77e4b04 @fields=[:a, :b, :c, :d, :e], 
      #      @data=[1, 2, 3, 4, 5], @tags=["cat", "dog"]>
      #
      #   >> Ruport::DataRow.new({ :a => 'moo', :c => 'caw'} , [:a,:b,:c,:d,:e],
      #      :tags => %w[cat dog])
      #   => #<Ruport::DataRow:0xb77c298c @fields=[:a, :b, :c, :d, :e],
      #      @data=["moo", nil, "caw", nil, nil], @tags=["cat", "dog"]>
      #
      #   >> Ruport::DataRow.new [1,2,3], [:a,:b,:c,:d,:e], :tags => %w[cat dog],
      #      :filler => 0
      #   => #<Ruport::DataRow:0xb77bb4d4 @fields=[:a, :b, :c, :d, :e], 
      #       @data=[1, 2, 3, 0, 0], @tags=["cat", "dog"]>
      #
      def initialize( data, fields, options={} )
        @fields   = fields
        @tags     = options[:tags] || {}
        @data     = []
        nr_action =  
          if data.kind_of?(Array)
            lambda { |key, index| @data[index] = data.shift || options[:filler] }
          elsif data.kind_of?(DataRow)
            lambda { |key, index| @data = data.to_a }
          else
            lambda { |key, index| @data[index] = data[key] || options[:filler] }
          end     
        @fields.each_with_index { |key, index| nr_action.call(key,index) }
      end
      
      attr_accessor :fields, :tags

      # Returns an array of values. Should probably return a DataRow.
      # Loses field information.
      def +(other)
        self.to_a + other.to_a
      end

      # Lets you access individual fields
      #
      # i.e. row["phone"] or row[4]
      def [](key)
        key.kind_of?(Fixnum) ? @data[key] : @data[@fields.index(key)] 
      end
      
      # Lets you set field values
      #
      # i.e. row["phone"] = '2038291203', row[7] = "allen"
      def []=(key,value)
        if key.kind_of?(Fixnum)
          @data[key] = value 
        else
          @data[@fields.index(key)] = value
        end                          
      end

      # Converts the DataRow to a plain old Array
      def to_a
        @data
      end

      # Converts the DataRow to a string representation
      # for outputting to screen.
      def to_s
        "[" + @data.join(",") + "]"
      end
      
      # Checks to see row includes the tag given.
      # 
      # Example:
      #
      #   >> row.has_tag? :running_balance
      #   => true
      #
      def has_tag?(tag)
        @tags.include?(tag)
      end
      
      # Iterates through DataRow elements.  Accepts a block.
      def each(&action)
        @data.each(&action)
      end
      
      # Allows you to add a tag to a row.
      #
      # Examples:
      #
      #   row.tag_as(:jay_cross) if row["product"].eql?("im_courier")
      #   row.tag_as(:running_balance) if row.fields.include?("RB")
      #
      def tag_as(something)
        @tags[something] = true
      end

      # Compares two DataRow objects. If values and fields are the same
      # (and in the correct order) returns true.  Otherwise returns false.
      def ==(other)
        self.to_a.eql?(other.to_a) && @fields.eql?(other.fields)
      end
      
      # Synonym for DataRow#==
      def eql?
        self == other
      end
    end
end
