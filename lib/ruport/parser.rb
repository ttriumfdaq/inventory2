#!/usr/local/bin/ruby -w
#  parser.rb,  A tool for parsing arbitrary input
#   ______________________         _ _
#  (Here there be dragons!) o    /    \
#   ----------------------    o | O  O |
#                               |      |  
#         .----.                \/\/\/\/        
#        | 0  0 |
#        |      | o   _____________________________                               
#        |/\/\/\|  o ( Dude. That phrase is sooo '89)
#                     ------------------------------
#
#   [ but seriously.  be careful in here. ]
#
#  This is Free Software, you may freely modify and/or distribute it by the
#  terms of the GNU General Public License or the Ruby license.
#
#  See LICENSE and COPYING for details.
#
#  Based on Parse::Input (http://rubyforge.org/projects/input)
#  Modified version created on 2006.02.20 by Gregory Brown
#  Copyight (C) 2006, all rights reserved.
#
#  Modifications from Parse::Input include:
#  - Ruport::Parser can handle string input
#  - Ruport::Parser does not define a top level input() method
#  - Any and all documentation has been added post-fork
#  - Ruby Style indentation used
#
#  Original application: 
#  Created by James Edward Gray II on 2005-08-14.
#  Copyright 2005 Gray Productions. All rights reserved.

require "stringio"

module Ruport
  
# If you're reporting on data, there is a good chance it's going to need some
# munging.  A 100% compatible modified version of JEG2's Parse::Input, Ruport::Parser
# can help you do that.
#
# This class provides a system that lets you specify regular expressions to
# identify patterns and extract the data you need from your input.
#
# It implements a somewhat straightforward domain specific language to help
# you scrape and tear up your gnarly input and get something managable.
#
# It is a bit of an expert tool, but is indispensible when messy data needs to be
# parsed. For now, you'll probably want to read the source for this particular
# class.  There may be dragons.
#
# Sample usage:
#  path = "somefile"
#  data = Ruport::Parser.new(path, "") do
#    @state = :skip
#    stop_skipping_at("Save Ad")
#    skip(/\A\s*\Z/)
#
#    pre { @price = @miles = nil }
#    read(/\$([\d,]+\d)/) { |price| @price = price.delete(",").to_i }
#    read(/([\d,]*\d)\s*m/) { |miles| @miles = miles.delete(",").to_i }
#
#    read do |ad|
#      if @price and @price < 20_000 and @miles and @miles < 40_000
#        (@ads ||= Array.new) << ad.strip
#      end
#    end
#  end 
  class Parser

    def initialize( path, separator = $/, &init )
      @path      = path
      @separator = separator
      @state     = :read

      @_pre_readers   = Array.new
      @_readers       = Array.new
      @_post_readers  = Array.new
      @_skip_starters = Array.new
      @_skip_stoppers = Array.new
      @_skips         = Array.new
      @_skip_searches = Array.new
      @_stops         = Array.new
      @_origin        = :file  
      instance_eval(&init) unless init.nil?
    
      parse
    end
  
    def []( name )
      variable_name = "@#{name}"
      if instance_variables.include? variable_name
        instance_variable_get(variable_name)
      else
        nil
      end
    end
  
    def method_missing( method, *args, &block )
      variable_name = "@#{method}"
      if instance_variables.include? variable_name
        instance_variable_get(variable_name)
      else
        super
      end
    end
  
    private
  
    def parse
      io_klass = (@origin.eql? :string) ? StringIO : File
      io_klass.open(@path) do |io|
        while @read = io.gets(@separator)
          if @_stops.any? { |stop| @read.index(stop) }
            @state = :stop
            break
          end

          case @state
          when :skip
            search(@_skip_searches)
          
            if @_skip_stoppers.any? { |stop| @read.index(stop) }
              @state = :read
            end
          when :read
            if @_skip_starters.any? { |start| @read.index(start) }
              @state = :skip
              search(@_skip_searches)
              next
            end
          
            if @_skips.any? { |skip| @read.index(skip) }
              search(@_skip_searches)
              next
            end

            @_pre_readers.each { |pre| instance_eval(&pre) }
            search(@_readers)
            @_post_readers.each { |post| instance_eval(&post) }
          end
        
          break if @state == :stop
        end
      end
    end
  
    def pre( &pre_readers_handler )
      @_pre_readers << pre_readers_handler
    end
  
    def read( pattern = nil, &handler )
      @_readers << Array[pattern, handler]
    end

    def post( &post_readers_handler )
      @_post_readers << post_readers_handler
    end
  
    def search( searches )
      searches.each do |pattern, handler|
        if pattern.nil?
          handler[@read]
        elsif pattern.is_a? Regexp
          next unless md = @read.match(pattern)
          captures = md.captures
          if captures.empty?
            handler[@read]
          else
            handler[*captures]
          end
        elsif @read.index(pattern)
          handler[@read]
        end
      end
    end
  
    def find_in_skipped( pattern = nil, &handler )
      @_skip_searches << Array[pattern, handler]
    end

    def skip( pattern )
      @_skips << pattern
    end
  
    def start_skipping_at( pattern )
      @_skip_starters << pattern
    end

    def stop_skipping_at( pattern )
      @_skip_stoppers << pattern
    end

    def stop_at( pattern )
      @_stops << pattern
    end

    def origin(source=:string)
      @_origin = :file
    end
  end
end
