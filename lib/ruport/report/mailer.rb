# mailer.rb
#  Created by Gregory Brown on 2005-08-16
#  Copyright 2005 (Gregory Brown) All Rights Reserved.
#  This product is free software, you may distribute it as such
#  under your choice of the Ruby license or the GNU GPL
#  See LICENSE for details
require "net/smtp"
module Ruport  
  class Report
    class Mailer
      
      def initialize( mailer_label=:default )
        select_mailer(mailer_label)
        @body = ""
        @recipients = []
      end
      
      #A list of email addresses to send the message to.
      attr_accessor :recipients
      
      #The body of the message to be sent
      attr_accessor :body

      # This takes _report_name_ as argument and sends the contents of @body to
      # @recipients
      def send_report(report_name="No Subject")
        return if @body.empty?
        Net::SMTP.start(@host,@port,@host,@user,@password,@auth) do |smtp|
          smtp.send_message( "Subject: #{report_name}\n\n#{@body}",
                             @address, 
                             @recipients )
        end
      end

      def select_mailer(label)
        mailer      = Ruport::Config.mailers[label]
        @host       = mailer.host
        @user       = mailer.user
        @password   = mailer.password
        @address    = mailer.address
        @port       = mailer.port       || 25
        @auth       = mailer.auth_type  || :plain
      end
        
      
    end
  end
end
