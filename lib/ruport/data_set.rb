# data_set.rb : Ruby Reports core datastructure.
#
# Author: Gregory T. Brown (gregory.t.brown at gmail dot com)
#
# Copyright (c) 2006, All Rights Reserved.
#
# This is free software.  You may modify and redistribute this freely under
# your choice of the GNU General Public License or the Ruby License. 
#
# See LICENSE and COPYING for details
module Ruport
  
  # The DataSet is the core datastructure for Ruport.  It provides methods that
  # allow you to compare and combine query results, data loaded in from CSVs,
  # and user-defined sets of data.  
  #
  # It is tightly integrated with Ruport's formatting and query systems, so if
  # you'd like to take advantage of these models, you will probably find DataSet
  # useful.
  #
  # Sample Usage:
  #
  #   my_data = [[1,2,3],[4,5,6],[7,8,9]]
  #
  #   ds = Ruport::DataSet.new([:col1, :col2, :col3],my_data)
  #   ds << [ 10, 11, 12]
  #   ds << { :col3 => 15, :col1 => 13, :col2 => 14 }
  #   puts ds.select_columns(:col1, :col3).to_csv
  # 
  # Output:
  #   
  #     col1,col3
  #     1,3
  #     4,6
  #     7,9
  #     10,12
  #     13,15
  #
  # The wild and crazy might want to try the Array hack:
  #
  #   puts [[1,2,3],[4,5,6],[7,8,9]].to_ds(%w[col1 col2 col3])
  #
  # Output:
  #
  #     fields: ( col1, col2, col3 )
  #     row0: ( 1, 2, 3 )
  #     row1: ( 4, 5, 6 )
  #     row2: ( 7, 8, 9 )
  #  
  class DataSet
    
    include Enumerable
    
    # DataSets must be given a set of fields to be defined.
    #
    # These field names will define the columns for the DataSet and how you
    # access them.
    #
    #    data = Ruport::DataSet.new %w[ id name phone ]
    #
    # You can optionally pass in some content as well. (Must be Enumerable)
    #
    #    content = [ %w[ a1 gregory 203-525-0523 ], 
    #                %w[ a2 james   555-555-5555 ] ]
    #
    #    data    = Ruport::DataSet.new(%w[ id name phone],content)
    def initialize(fields=[], content=nil, default=nil)
      @fields = fields
      @data = []
      @default = default
      content.each { |r| self << r } if content
    end
    
    #an array which contains column names
    attr_accessor :fields
    
    #the default value to fill empty cells with 
    attr_accessor :default
    
    #data holds the elements of the Row
    attr_reader   :data
   
    #provides a deep copy of the DataSet. 
    def clone
      DataSet.new(@fields,@data)
    end
    
    #Allows ordinal access to rows
    #
    #   my_data[2] -> Ruport::DataRow
    def [](index)
      @data[index]
    end
    
    #allows setting of rows (providing a DataRow is passed in)
    def []=(index,value)
      throw "Invalid object type" unless value.kind_of?(DataRow)
      @data[index] = value
    end

    # appends a row to the DataSet
    # can be added as an array or a keyed hash-like object.
    # 
    # Columns left undefined will be filled with DataSet#default values.
    # 
    #    data << [ 1, 2, 3 ]
    #    data << { :some_field_name => 3, :other => 2, :another => 1 }
    def << ( stuff, filler=@default )
      @data << DataRow.new(stuff,@fields,:filler => filler)
    end

    # checks if one dataset equals another
    def eql?(data2)
      return false unless ( @data.length == data2.data.length and
                            @fields.eql?(data2.fields) )
      @data.each_with_index do |row, r_index|
        row.each_with_index do |field, f_index|
          return false unless field.eql?(data2[r_index][f_index])
        end
      end

      return true
    end

    # checks if one dataset equals another
    def ==(data2)
      eql?(data2)
    end

    # Returns true if DataSet contains no rows, false otherwise.
    def empty?
      return @data.empty?
    end

    # Allows loading of CSV files or YAML dumps. Returns a DataSet
    #
    # FasterCSV will be used if it is installed.
    #
    #   my_data = Ruport::DataSet.load("foo.csv")
    #   my_data = Ruport::DataSet.load("foo.yaml")
    #   my_data = Ruport::DataSet.load("foo.yml")
    def self.load ( source, default="")
      case source
      when /\.(yaml|yml)/
        return YAML.load(File.open(source))
      when /\.csv/ 
        csv_klass = defined?(FasterCSV) ? FasterCSV : CSV
        input = csv_klass.read(source) if source =~ /\.csv/
        loaded_data = self.new
        loaded_data.fields = input[0]
        loaded_data.default = default
        input[1..-1].each { |row| loaded_data << row }
        return loaded_data	
      else
        raise "Invalid file type"
      end
    end
   
    # Iterates through the rows, yielding a DataRow for each.
    def each(&action)
      @data.each(&action)
    end

    # Returns a new DataSet composed of the fields specified.
    def select_columns(*fields)
      rows = fields.inject([]) { |s,e| s << map { |row| row[e] } }.transpose
      my_data = DataSet.new(fields,rows)
    end
    
    # Returns a new DataSet with the specified fields removed
    def remove_columns(*fields)
      select_fields(*(@fields-fields))
    end

    # removes the specified fields from this DataSet (DESTRUCTIVE!)
    def remove_columns!(*fields)
      @fields -= fields
      @data = select_fields(*(@fields)).to_a
    end

    # uses Format::Builder to render DataSets in various ready to output
    # formats.  
    #
    #    data.as(:html)                  -> String
    #
    #    data.as(:text) do |builder|
    #      builder.range = 2..4          -> String
    #      builder.header = "My Title"
    #    end
    #
    # To add new formats to this function, simply re-open Format::Builder
    # and add methods like <tt>render_my_format_name</tt>. 
    #
    # This will enable <tt>data.as(:my_format_name)</tt>
    def as(format,&action)
      builder = Format::Builder.new( self )
      builder.format = format
      action.call(builder) if block_given?
      builder.render
    end
    
    # Converts a DataSet to CSV
    def to_csv; as(:csv) end

    # Converts a Dataset to html
    def to_html; as(:html) end
    
    # Readable string representation of the DataSet
    def to_s; as(:text) end
  end
end

class Array
  
  # Will convert Arrays of Enumerable objects to DataSets. 
  # May have dragons.
  def to_ds(fields,default=nil)
    Ruport::DataSet.new(fields,to_a,default)
  end
 
end
