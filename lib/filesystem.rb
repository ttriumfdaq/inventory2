#Contains generalized methods for controllers that generate and
#interact with a list of files
#(designed to be used as a  mixin)
module Filesystem

  #returns an array with arr[0] = total number of bytes in the
  #directory and arr[1] = total number of files in the directory
  #arr[2] = time object representing the last time that a file was 
  #added to the directory
  def self.stats(directory)
    arr = Array.new
    arr[0] = 0
    arr[1] = 0
    arr[2] = nil
    get_files(directory).each do |filename| 
      arr[0] += File.size(directory + filename)
      arr[1] += 1
      arr[2] = File.mtime(directory + filename) if arr[2] == nil || File.mtime(directory + filename) > arr[2]
    end
    return arr
  end
  
  #returns all files in the directory that are of the proper extensions
  def self.get_files(directory, extensions = ['.zip'])
    files = Array.new
    Dir.entries(directory).each do |entry| 
      files << entry if extensions.include?(File.extname(entry))
    end
    files.sort!.reverse!
  end
  
  #returns true if the file exists, false if it does not
  def self.file_exists?(directory, extensions, file_name)
   backup_files = get_files(directory,extensions)
   return backup_files.include?(file_name)
  end
  
end