require File.dirname(__FILE__) + '/../test_helper'
require 'support_mailer'

class SupportMailerTest < Test::Unit::TestCase
  FIXTURES_PATH = File.dirname(__FILE__) + '/../fixtures'
  CHARSET = "utf-8"

  include ActionMailer::Quoting

  def setup
    ActionMailer::Base.delivery_method = :test
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []

    @expected = TMail::Mail.new
    @expected.set_content_type "text", "plain", { "charset" => CHARSET }
  end

  def test_request
    @expected.subject = 'SupportMailer#request'
    @expected.body    = read_fixture('request')
    @expected.date    = Time.now

    assert_equal @expected.encoded, SupportMailer.create_request(@expected.date).encoded
  end

  def test_error
    @expected.subject = 'SupportMailer#error'
    @expected.body    = read_fixture('error')
    @expected.date    = Time.now

    assert_equal @expected.encoded, SupportMailer.create_error(@expected.date).encoded
  end

  private
    def read_fixture(action)
      IO.readlines("#{FIXTURES_PATH}/support_mailer/#{action}")
    end

    def encode(subject)
      quoted_printable(subject, CHARSET)
    end
end
