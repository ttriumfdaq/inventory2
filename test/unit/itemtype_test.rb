require File.dirname(__FILE__) + '/../test_helper'

class ItemTypeTest < Test::Unit::TestCase
  fixtures :itemtypes

  # Replace this with your real tests.
  def test_truth
    assert_kind_of ItemType, itemtypes(:first)
  end
end
