require File.dirname(__FILE__) + '/../test_helper'

class KindTest < Test::Unit::TestCase
  fixtures :kinds

  # Replace this with your real tests.
  def test_truth
    assert_kind_of Kind, kinds(:first)
  end
end
