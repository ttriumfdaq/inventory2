class AccountController < ApplicationController
 
  before_filter :login_required, :except => [:login, :config, :existing_users, :new_user, :destroy_user]
  before_filter :root_login_required, :except => [ :login, :change_password, :logout ]
  
  #interceptor page that forces users to login
  def login
  end

  #login page
  def login
    @user = User.new(params[:user])
    case
    #handles not logged in
    when (!session[:user] || (!session[:user] and params[:root_required]))
      if request.post?
        if logged_in_user = @user.try_to_login
          session[:user] = logged_in_user
          LogEntry.create('login succeeded for user "' + @user.user_name + '"')
          redirect_to session[:redirect_where_after_login]
        else
          flash.now[:login_error] = "Invalid username / password."
          LogEntry.create('login failed for user "' + @user.user_name + '"')
        end
      else
        if params[:root_required]
          flash.now[:login_prompt] = "Please login as root." 
          @user.user_name = 'root'
        else
          flash.now[:login_prompt] = "Please enter your username and password." 
        end
      end
    #handles logged in (not as root) and root required
    when (session[:user] and session[:user].user_name != 'root' and params[:root_required])
      if request.post?
        if User.root_password_correct?(params[:root_password])
          session[:user].has_root = true
          LogEntry.create('root acquisition succeeded')
          redirect_to session[:redirect_where_after_login]
        else
          LogEntry.create('root acquisition failed')
          flash.now[:login_error] = "The root password you entered is incorrect."
        end
      else
        flash.now[:login_prompt] = "Please enter the root password."
      end
    else
      redirect_to_index('Already logged in.')
    end
  end
        

  
 
  #allows all users to change their own password
  def change_password
    if request.post?
      @user = User.find(session[:user].id)
      @user.old_password = params[:user][:old_password]
      @user.password = params[:user][:password]
      @user.password_confirmation = params[:user][:password_confirmation]
      if @user.save
        redirect_to_index "Password changed" 
      end
    else
      @user = User.new 
    end
    
  end 
 
  
  def logout
    @session[:user] = nil 
    if params['referring_controller'] == 'config' || params['referring_controller'] == 'account'
      redirect_to :controller => 'frontend', :action => 'index'
    else
      redirect_to :back
    end
  end
  
  #if a normal user is currently logged in, this gets rid of their root privileges
  def relinquish_root
    @session[:user].has_root = nil
    if params['referring_controller'] == 'config' || params['referring_controller'] == 'account'
      redirect_to :controller => 'frontend', :action => 'index'
    else
      redirect_to :back
    end
  end
  
  
  
  
  #displays the list of existing users and handles changes and additions to the user
  #table that are submitted back to itself (the users action)
  def config
    render :layout => 'config_layout'
  end
  
  #ajax component for config
  def existing_users
    @users = User.find(:all)
    if request.xhr?
      if params[:commit] == 'update'
        User.disable_old_password_validation
        @users.each { |u| (@user = u) if (u.id == params[:user_to_edit].to_i) }
        raise "User not found" unless @user
        @user.attributes = params[:user]
        if @user.save
          redirect_to(:action => 'existing_users') 
        else
          render :layout => false
        end
      elsif params[:commit] == 'delete'
        #this exception prevents everyone from deleting the root user
        begin
          
          if params[:user_to_edit].to_i == session[:user].id.to_i
            flash.now[:existing_users_notice] = ('You cannot delete your own user.')
            render :layout => false
          else
            User.destroy(params[:user_to_edit])
            redirect_to(:action => 'existing_users') 
          end
        rescue RuntimeError
          flash.now[:existing_users_notice] = $!
          render :layout => false
        end


      elsif params[:commit] == 'close'
        redirect_to(:action => 'existing_users') 
      else
        render :layout => false
      end  
    else
      render :layout => false
    end
  end
  
 
  
  #component for config
  def new_user
    @user = User.new(params[:user])
    @user.user_name = params[:user][:user_name] if params[:user]
    if request.xhr?
        User.disable_old_password_validation
        if @user.save
          ##flash.now[:new_user_notice] = "User #{@user.login} has been created"
          @user = User.new
          render :status => 200, :layout => false
        else
          render :status => 500, :layout => false
        end  
    else
      render :layout => false
    end
  end
 
  
 

end
