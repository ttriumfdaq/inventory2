#######MAKE PASSWORD EDITING CLEAR OUT THE SESSIONS TO KICK OFF ANY CURRENTLY LOGGED ON USERS!


class ConfigController < ApplicationController

  FORBIDDEN_FIELD_NAMES = %w{ action application dispatcher display format load new notify open quote request records send session system template test type created_at created_on updated_at updated_on lock_version type id position parent_id lft rgt additional_load_paths argf argv actioncontroller actionview activerecord argumenterror array basicsocket benchmark bignum binding cgi cgimethods cross_compiling class classinheritableattributes comparable conditionvariable config continuation drb drbidconv drbobject drbundumped data dat datetime delegater delegator digest dir env eoferror erb enumerable errno exception false falseclass fcntl file filelist filetask filetest fileutils fixnum float floatdomainerror gc gem getoptlong hash io ioerror ipsocket ipsocket indexerror inflector integer interrupt kernel ln_supported loaderror localjumperror logger marshal matchdata matchingdata math method module mutex mysql mysqlerror mysqlfield mysqlres nil nameerror nilclass nomemoryerror nomethoderror nowrite notimplementederror numeric opt_tabe object objectspace observable pgerror pgconn pglarge pgresult platform pstore parsedate precision proc process queue rakeversion release_date ruby ruby_platform ruby_release_date ruby_version rake rakeapp rakefileutils range rangeerror rational regexp regexperror request runtimeerror stderr stdin stdout scanerror scripterror securityerror signal signalexception simpledelegater simpledelegator singleton sizedqueue socket socketerror standarderror string stringscanner struct symbol syntaxerror systemcalleror systemexit systemstackerror tcpserver tcpsocket tcpserver tcpsocket toplevel_binding true task text thread threaderror threadgroup time transaction trueclass typeerror udpsocket udpsocket unixserver unixsocket unixserver unixsocket unboundmethod version verbose yaml zerodivisionerror add all alter analyze and as asc asensitive before between bigint binary blob both by call cascade case change char character check collate column condition connection constraint continue convert create cross current_datecurrent_time current_timestamp current_user cursor database databases day_hour day_microsecond day_minute day_second dec decimal declare default delayed delete desc describe deterministic distinct distinctrow div double drop dual each else elseif enclosed escaped exists exit explain false fetch float float4 float8 for force foreign from fulltext grant group having high_priority hour_microsecond hour_minute hour_second if ignore in index infile inner inout insensitive insert int int1 int2 int3 int4 int8 ineger interval into is iterate join key keys kill leading leave left like limit lines load localtime localtimestamp lock long longblob longtext loop low_priority match mediumblob mediumint mediumtext middleint minute_microsecond minute_second mod modifies natural not no_write_to_binlog null numeric on optimize option optionally or order out outer outfile precision primary procedure purge raid0 read reads real references regexp release rename repeat replace require restrict return revoke right rlike schema chemas second_microsecond select sensitive separator set show smallint soname spatial specific sql sqlexception sqlstate sqlwarning sql_big_result sql_calc_found_rows sql_small_result ssl starting straight_join table terminated then tinyblob tinyint tinytext to trailing trigger true undo union unique unlock unsigned update upgrade usage use using utc_date utc_time utc_timestamp values varbinary varchar varcharacter varying when where while with write x509 xor year_month zerofill }

  #cache_sweeper :category_sweeper

  before_filter :root_login_required, :except => [ :trigger ]

  layout 'config_layout', :except => [:existing_users, :new_user]
  
  def index
    redirect_to :action => :backup
  end

  #responds to ajax request for editing categories and also displays the initial list
  #when it is called via render_component
  def view_categories
    #if a category has been selected for modification, load it up
    if params[:id]
      @category = Category.find(params[:id])
    end
    
    
    
    render :layout => false
  end
  
  def categories
  end
  
  #carries out ajax requests to add, edit and delete categories before redirecting to
  #view_categories
  def affect_category_ajax
    expire_fragment(/./)
    @category = Category.find(params[:id]) if params['ajax_action'] == 'edit' || params['ajax_action'] == 'delete'
    case params['ajax_action']
      when 'edit'
        old_name = @category.name
        if @category.update_attributes(params[:category])
          render_component :action => "view_categories"
        else
          render :partial => 'categories_error', :status => 500, :locals => {:ajax_action => 'edit', :category => old_name}
        end
      when 'delete'
        begin
          @category.destroy
          render_component :action => "view_categories"
          session[:open_cat] = nil
          rescue StandardError
            STDERR.puts "$! #{$!}"
            if $!.to_s == 'category not empty'
              render_component :action => "view_categories", :status => 500
              response.headers['Status'] = '500'
            else
              raise
            end
        end
      when 'new'
        @category = Category.new(params[:category])
        @category.description = ''
        @category.displayed_fields = ''
        @category.hidden_fields = ''
        if @category.save
          render_component :action => "view_categories"
        else
          render :partial => 'categories_error', :status => 500, :locals => {:ajax_action => 'new'}
        end
      else
        render :nothing => true, :status => 500
      end
      
  end
  
  def fields
    @status_options = Item.load_status_options
  end

  #changes the YAML setting that contains the options that are in the status
  #dropdown menu in the list view
  def status_options
    
    @status_options = Item.load_status_options
    
    if request.xhr?
      case params[:do]
      when 'create'
        #ensure that the input only consists of letters, spaces and commas
        if params[:option].count("a-zA-Z ") == params[:option].length
          if params[:option] == nil || params[:option].length == 0
            @error = "You must enter a name for the status option"
          elsif @status_options.include?(params[:option])
            @error = "That status option already exists."
          else
            @status_options << params[:option]
            Settings.status_options = YAML::dump(@status_options)
            LogEntry.create("status option #{params[:option]} created")
          end
        else
          @error = "Status options may only consist of uppercase characters, lowercase characters and spaces."
        end
      when 'delete'
        @status_options.delete_if{|item| item == params[:option]}
        Settings.status_options = YAML::dump(@status_options)   
        LogEntry.create("status option #{params[:option]} deleted")
      end
    end
  end
  
  #processes any fields update requests that are coming in via AJAX
  #then redirects to view_fields
  def ajax_fields
    if params['table'] == 'kinds' and params['update_field'] == 'name'
      flash[:notice] = "The field '#{params['update_field']}' cannot be modified." 
    else
      #if the table name is not items or kinds... explode. boom.
      raise "InvalidTableName" if !['items','kinds'].include?(params['table'])
      

      if FORBIDDEN_FIELD_NAMES.include?(params['update_field']) || FORBIDDEN_FIELD_NAMES.include?(params['new_field'])

        flash[:notice] = "Error: the field name you specified belongs to the list of forbidden field names (#{FORBIDDEN_FIELD_NAMES.join(', ')})."
      else
        #takes an ajax request to change the name of a field and changes it if feasible
        if params['update_field']
          if get_columns.include?(params['update_field'])
            field = params['update_field']
            value = params['value']    
            if value =~ /\A([a-z]|_)+\z/
              Item.connection.rename_column(params['table'],field,value)
              flash[:notice] = "Field #{params['update_field']} renamed to #{params['value']}"
            else
              flash[:notice] = "Error: field name can only consist of consist of lowercase letters and underscore characters."
            end
            #expire_fragment(/./)
            
          else
            raise "OldColumnNameNotFound"
          end
        end  
      
        #if we just received a request to create a new field, if the field doesn't already exist then create it
        if params['new_field']
          if params['new_field'] !~ /\A([a-z]|_)+\z/
            flash[:notice] = "Error: field name can only consist of consist of lowercase letters and underscore characters."
          elsif get_columns.include?(params['new_field'])
            flash[:notice] = "Error, that field name already exists."
          else
            Item.connection.add_column(params['table'], '`'+params['new_field']+'`', :text)
            #expire_fragment(/./)
            flash[:notice] = "Field #{params['new_field']} created."
            LogEntry.create("created field #{params['new_field']} in table #{params['table']}")
          end
        end
      end
      
      #if we just received a request to delete a field, then only
      #delete it if no items use that field
      if params['delete_field']
        if params['delete_field'] == ''
          flash[:notice] = "Error, no field to delete was specified."
        elsif get_columns.include?(params['delete_field'])
        
          if params['table'] == 'items'
            num_items_using_field = Item.count("`#{params['delete_field']}` IS NOT NULL")
          else
            num_items_using_field = Item.find(:all, :include => 'kind', :conditions => "kinds.#{params['delete_field']} IS NOT NULL").size
          end
        
          if num_items_using_field == 0
            Item.connection.remove_column(params['table'], '`'+params['delete_field']+'`')
            #expire_fragment(/./)
            flash[:notice] = "Field #{params['delete_field']} deleted."
            LogEntry.create("deleted data field #{params['delete_field']} in table #{params[:table]}")
          else
            flash[:notice] = "Error: field #{params['delete_field']} could not be deleted because it contains #{num_items_using_field} items."
          end
        end
      end 
      Item.reset_column_information
      Kind.reset_column_information
    end

    redirect_to :action => 'view_fields', :table => params['table']
    

  end
  
  
  #component that renders the table of fields
  def view_fields
    @columns = get_columns
    render :layout => false
  end
  
  #changes the number of log items displayed per page and displays the log
  def log  
    if ['10','100','1000','10000'].include?(params['number_of_log_items_to_display'])
      Settings.number_of_log_items_to_display = params['number_of_log_items_to_display']
      redirect_to :action => :log
    end
    @log_entry_pages, @log_entries = paginate :log_entries, :per_page => Settings.number_of_log_items_to_display.to_i, :order => "created_on DESC"
  end
  

  def backup
    @stats = Filesystem.stats(Backup.backups_dir)
    @backup_files_pages, @backup_files = paginate_collection Filesystem.get_files(Backup.backups_dir), :page => params[:page]
    @backup_storage_location = Settings.backup_storage_location
    @backup_interval = Settings.backup_interval
    rescue Errno::ENOENT
      flash[:backup_error] = "Backup file storage location could not be accessed. Check setup below."
      
  end 
  
  #triggers the creation of a new backup
  def new_backup
    filename = Backup.new
    flash[:notice] = "Database and attachments backed up to #{filename}"
    redirect_to :back
  end 
  
  #triggers the restoration of a backup file
  def restore_backup
    file_restored = Backup.restore(params[:id])
    flash[:notice] = 'The application has been restored with data from ' + params[:id]
    #delete all cached fragments.  we don't want to be cacheing stuff that is form an old version of the db.
    expire_fragment(/./)
    redirect_to :back
  end
  
  #changes the backup interval preference
  def change_backup_interval
    if ['disabled', 'hourly','daily','weekly','monthly'].include?(params['backup_interval'])
      Settings.backup_interval = params['backup_interval']
      flash[:notice] = 'Backup interval updated.'
    else
      flash[:notice] = "Backup interval must be one of 'disabled', 'hourly','daily','weekly' or 'monthly'"
    end
      redirect_to :back
  end
  
  #changes the backup storage location preference
  def change_backup_storage_location
    case
      when !File.directory?(params['backup_storage_location'])
        flash[:notice] = 'Directory does not exist.'
      when !File.writable?(params['backup_storage_location'])
        flash[:notice] = 'Directory not writable by the inventory system. Check permissions.'
      else
        params['backup_storage_location'].chop! if params['backup_storage_location'][-1] == '/'
        Settings.backup_storage_location = params['backup_storage_location']
        flash[:notice] = 'Backup storage location updated.'
      end
      redirect_to :back
  end

  
  #this method is called every 30 seconds from a cron job to perform
  #scheduled backups
  def trigger
    
    latest_backup_time = Filesystem.stats(Backup.backups_dir)[2]
    backup_interval = Settings.backup_interval
    perform_backup = false
    if backup_interval == 'disabled'
      text = "Backups are disabled in the preferences. \n"
    else
      #if there aren't any backups yet then perform a first backup regardless of latest_backup_time
      if latest_backup_time == nil
          text = "There are currently no backups in the backup directory so an inital backup is being performed.\n"
          perform_backup = true
      else
        seconds_since_last_backup = Time.now - latest_backup_time
        minutes_since_last_backup = seconds_since_last_backup / 60
        hours_since_last_backup = minutes_since_last_backup / 60
        days_since_last_backup = hours_since_last_backup / 24
        weeks_since_last_backup = days_since_last_backup / 7
        months_since_last_backup = days_since_last_backup /30
        text = "time now: #{Time.now.to_s}\nlatest_backup_time: #{latest_backup_time.to_s}\n"
        text += "time since last backup: #{seconds_since_last_backup} seconds \n";
        text += "time since last backup: #{minutes_since_last_backup} minutes \n";
        text += "time since last backup: #{hours_since_last_backup} hours \n";     
        text += "time since last backup: #{days_since_last_backup} days \n"; 
        text += "time since last backup: #{weeks_since_last_backup} weeks \n";        
        text += "time since last backup: #{months_since_last_backup} months \n";       
        #decide whether or not to perform a backup depending on wether or not
        #enough time has elapsed since the last backup
        case backup_interval
          when 'hourly'
            perform_backup = true if hours_since_last_backup > 1
          when 'daily'
            perform_backup = true if days_since_last_backup > 1
          when 'weekly'
            perform_backup = true if weeks_since_last_backup > 1
          when 'monthly'
            perform_backup = true if months_since_last_backup > 1
        end
      end
    end
    if perform_backup
      file_name = Backup.new 
      text +=  "Backup performed (#{file_name}). \n"
    else
       text += "No backup performed. \n"
    end
    STDERR.puts text
    render(:text => text)
  end
  
  #streams a backup file from a location on the server filesystem
  #back to the client.  The point of this is that it makes it
  #easy to restrict access to backup files to logged-in users.
  def stream_backup_file
    send_file(Backup.stream_backup_file(params[:id]))
  end
  
  def setup
    if request.post?
      Settings.home_title = params[:home_title]
      Settings.navbar_title = params[:navbar_title]
      
      Settings.email_address = params[:email_address]
      Settings.email_cc = params[:email_cc] 
    
      Settings.allow_item_deletion = params[:allow_item_deletion]
      Settings.allow_note_deletion = params[:allow_note_deletion]
      Settings.allow_pagination = params[:allow_pagination]
      
      Settings.history_enabled = params[:history_enabled]
      Settings.notes_enabled = params[:notes_enabled]
      
      Settings.documentation_enabled = params[:documentation_enabled]
      Settings.images_enabled = params[:images_enabled]
      
      LogEntry.create("setup updated")
      redirect_to :action => :setup
    end
  end
  
  private
  
  #if int == 0 return false
  #else (presumably int == 1) return true
  def int_to_bool(int)
    (int == 0)?false:true
  end
  
  #convenience method that returns an array containing only the columns that 
  #exist and are user-accessible for updating
  #it will send back the columns for the correct table (items or kind)
  #according to whichever is selected through params['table']
  def get_columns
    #Resets all the cached information about columns, which will cause them 
    #to be reloaded on the next request.
    if params['table'] == 'items'
      #Item.reset_column_information
      return Item.content_columns_without_stamps
    elsif params['table'] == 'kinds'
      #Kind.reset_column_information
      return Kind.content_columns_without_stamps
    else
      return nil
    end
  end 
  
  #extends the paginator to work on collections (as opposed to just on activerecord)
  def paginate_collection(collection, options = {})
    default_options = {:per_page => 10, :page => 1}
    options = default_options.merge options
    pages = Paginator.new self, collection.size, options[:per_page], options[:page]
    first = pages.current.offset
    last = [first + options[:per_page], collection.size].min
    slice = collection[first...last]
    return [pages, slice]
  end
 
end
