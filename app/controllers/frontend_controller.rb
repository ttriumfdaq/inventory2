class FrontendController < ApplicationController
 
  before_filter :login_required, :except => [:breadcrumbs, :index, :search, :list, :view_list, :show, :show_type, :history, :rss, :save_display_settings, :support, :change_per_page, :help, :back_to_old_table, :back_to_new_table ]


  
  #asynchronously updates containers on the show page in response to ajax updates
  #performed by the user
  def update_show
    loaditem
  end

  def import
	token = (0...15).map{(65 + rand(26)).chr}.join
#	current_time = Time.now.strftime("%d/%m/%Y %H:%M")     don't need to add current time - auto done in mysql
#	Token.new({:token => token, :current_time => current_time})
	@the_token = Token.new({:token => token})
	@the_token.save
  
	redirect_to("https://ladd00.triumf.ca/~daqinv/public/index.php?token=#{token}")
  end

  def newtable
	redirect_to("https://ladd00.triumf.ca/~daqinv/inventory3/public/index.php/home")
  end

  def back_to_old_table
	redirect_to("https://ladd00.triumf.ca/daqinv/frontend/list/" + params[:id])
  end

  def back_to_new_table
	redirect_to("https://ladd00.triumf.ca/~daqinv/inventory3/public/index.php/daqinv/lists/" + params[:id])
  end

  def index
    @items = get_last_ten
    @number_of_items = Item.count
    @number_of_retired_items = Item.count("status = 'retired'")
    @number_of_non_retired_items = @number_of_items - @number_of_retired_items
    @number_of_categories = Category.count
    @number_of_users = User.count
    @number_of_files = Attachment.count
    @number_of_image_files = Attachment.count('image IS NOT NULL')
    @number_of_documentation_files = Attachment.count('documentation IS NOT NULL')
  end
  
  #sends an email to the admin
  def support
    if request.post?
      @feedback = Feedback.new(params['feedback'])
      if @feedback.valid?
        SupportMailer.deliver_request(request.remote_ip, @feedback.name, @feedback.email, @feedback.message)
        redirect_to_index("Thank you #{params['feedback']['name']}. Your message has been sent.")
      end
    end
  end
  
  #generates an rss feed of the last 10 items to be added to the system
  def rss
    @url_prefix = 'http://'+ request.host_with_port
    @results = get_last_ten
    render :layout => false
  end

  def search
    if params[:text] == nil
      flash.now[:notice] = "No search term"
    else
      order_by = get_order_by
      @items = Item.search( params[:text],:include => ['notes', 'kind'], :order => order_by )
      @columns_to_render = Category.default_colstodisplay(@items)
      flash.now[:notice] = "No items contain the text \"#{params[:text]}\"." if @items.size == 0
    end
  end

  #displays the list of items in a category
  def list
    #save the location we are currently at so that we remember which category to keep open
    session[:open_cat] = params[:id] 
    @current_category = Category.find(params[:id])
    load_resources_for_view_list
    load_hidden_cols
    merge_hidden_cols_with_defaults_ctd
    rescue ActiveRecord::RecordNotFound
      redirect_to_index("Category not found") 
  end

  #deal with any default display column changes  
  def save_preferences
    @category = Category.find(params[:id])
    @category.hidden_fields = YAML::dump((params[:hidden] || Array.new))
    @category.displayed_fields = YAML::dump((params[:displayed] || Array.new))
    @category.save
    @current_category = @category
    render_view_list
  end

  #saves a user's setting that are stored in the session (used for things like show
  #retired items) and redisplays view_list
  def save_display_settings
    session['show_retired'] = params['show_retired']
    session['show_all_fields'] = params['show_all_fields']
    @current_category = Category.find(params[:id])
    render_view_list
  end

  
  #changes the number of items displayed per list view page (setting stored in session)
  def change_per_page
    session['per_page'] = params['per_page']
    redirect_to :back
  end
  
  #processes requests from the list view where multiple items
  #are selected via radio buttons
  def batch_change
    if !params || !params['ids']
      flash[:batch_notice] = "No items selected"
    else
      @keys = params['ids'].keys
      case params['task']
        when 'delete'
          if Settings.allow_item_deletion.to_i == 1
            @keys.each{ |item_id| Item.destroy(item_id) }
          end
        when 'move'
          @keys.each{ |item_id| Item.find(item_id).update_attribute('category',Category.find(params['item']['category'])) }
        when 'change'
          raise 'InvalidStatusString' if !Settings.status_options.include?(params['item']['status'])
          @keys.each{ |item_id| Item.find(item_id).update_attribute('status',params['item']['status']) }
        when 'update'
          raise 'InvalidField' if !Item.content_columns_without_stamps_or_status.include?(params['field'])
          @keys.each{ |item_id| Item.find(item_id).update_attribute(params['field'],params['new_value']) }
        when 'clone'
          if ((1..999).include?(params["number_of_clones"].to_i)) and (params["number_of_clones"].to_s =~ /^[+-]?\d+$/)
            @keys.each do |item_id| 
              params["number_of_clones"].to_i.times do
                clone = Item.find(item_id).clone 
                clone.save
              end
            end
          else
            flash[:batch_notice] = "Quantity must be 1 - 999"
          end
        when 'change_type'
          if params['item']['kind'] == nil || params['item']['kind'] == '' || params['item']['kind'] == ' '
            flash[:batch_notice] = "No kind name specified"
          else
            @keys.each do |item_id| 
              item = Item.find(item_id)
              old_value = item.kind.name
              item.kind = Kind.find_or_create_by_name(params['item']['kind'])
              user = User.find(session[:user][:id])
              Change.create( :item => item, :user => user, :field => 'type_name', :old_value => old_value, :new_value => item.kind.name )
              item.save
            end
          end
      end
    end
  @current_category = Category.find(params[:id])
  render_view_list
  end
  

  
  #changes the status of an item
  def change_status
    loaditem
    if Settings.status_options.include?(params['status'])
      @item.update_attribute('status', params['status'])
    else
      raise "Invalid value for status"
    end
    render :layout => false
  end
  
  #moves an item to another category
  def change_category
    loaditem
    @category = Category.find(params['item']['category'])
    @item.category = @category
    @item.save
    redirect_to :back    
  end

  #shows the details for a particular item
  def show
    loaditem
    if @item
      @kind = @item.kind 
      @current_category = @item.category # necessary for load_displayed_cols
      load_displayed_cols
      load_hidden_cols
      load_unclassified_cols
      session[:open_cat] = @item.category.id 
    end
  end
  
  #ajax response method for updating fields in an item or its kind
  def update
    @item = Item.find(params[:id])
    
    case params['model']
    when 'item'
      model = @item
      raise "FieldNotFound" if !Item.content_columns.collect{|column| column.name}.include?(params[:attribute])
    when 'kind'
      model = @item.kind
      raise "FieldNotFound" if !Kind.content_columns.collect{|column| column.name}.include?(params[:attribute])
    else
      raise 'Incorrect model parameter'
    end
    
    old_value = model.send(params[:attribute])
    
    if model.update_attributes({ params[:attribute] => params[:value] } ) 
      render :text => params[:value]
    else
      if params[:model] == 'kind' and params['attribute'] == 'name'
        render :text => "there already exists a type with the name \"#{params[:value]}\".  Since type names must be unique, you need to choose a different type name. Alternatively, you may change this item's type (rather than just attempting to rename the current type) by clicking on \"change this item's type\".", :status => 500
      else
        render :text => 'field could not be saved.', :status => 500
      end
    end
    if params['model'] == 'item'
      user = User.find(session[:user][:id])
      Change.create( :item => @item, :user => user, :field => params[:attribute], :old_value => old_value, :new_value => params[:value] )
    end
  end    
  
  #called from the show view
  def clone_item
    clone = Item.find(params[:id]).clone 
    clone.save
    flash[:notice] = "This item is a new clone of item # #{params[:id]}."
    redirect_to :action => 'show', :id => clone.id
  end
  
  #called from the show view
  def new_item_of_same_type
    @old_item = Item.find(params[:id])
    @item = Item.new
    @item.category = @old_item.category
    @item.kind = @old_item.kind
    @item.save
    flash[:notice] = "This is a new item."
    redirect_to :action => 'show', :id => @item.id
  end
  
  #called from the show view
  def delete_item
    cat = Item.find(params[:id]).category.id
    if Settings.allow_item_deletion.to_i == 1
      Item.destroy(params[:id])
    else
      flash[:notice] = 'Item deletion is disabled.'
    end
    redirect_to :action => 'list', :id => cat
  end
  
  #component that renders the type data box for an item
  def show_type
    loaditem
    if @item
      @kind = @item.kind 
      session[:open_cat] = @item.category.id 
    end
    render :layout => false
  end
  
  
  #ajax action that allows us to change an item's type from the show page
  def change_type
    loaditem
    if params[:commit] == 'update'
      kind = Kind.find_or_create_by_name(params['item']['kind'])
      @item.change_kind(kind,session[:user].id)
      redirect_to :action => :show_type, :id => params[:id]
    else
      session[:open_cat] = @item.category.id       
      render :layout => false
    end

  end
  
  
  #a component that renders the item history 
  def history
    @history_items = Change.find_all_by_item_id(params[:id], :order=> 'id DESC')
    render :update do |page|
      page.replace_html  'history', :partial => 'history'
    end
  end
  
  #deals with ajax requests to add or delete notes
  def notes
    if request.xhr?
      case params[:do]
        when 'create'
          ActiveRecord::Base.record_timestamps = false
          loaditem
          @note = Note.new(params[:note])
          @note.user = User.find(session[:user].id)
          unless @item.notes << @note
            @errors = true
          end
        when 'delete'
          raise "deletions are not permitted" if Settings.allow_note_deletion != '1'  
          Note.destroy(params[:id])
      end
    end
  end

  #takes ajax request to make a new item from the list view
  def new
    @item = Item.new
    @item.category = Category.find(params[:category]) 
    #if a kind with this name exists already then just point to it
    @item.kind = Kind.find_by_name(params[:item][:kind]) 
    #else create a new kind with this name
    unless @item.kind
      @item.kind = Kind.new(:name => params[:item][:kind])    
    end
    @succeeded = @item.save
  end 
  

  #the method that responds to ajax calls from the new action with results from the kinds table
  def auto_complete_for_item_kind
    search = params[:item][:kind]
    @results =  Kind.search(search) unless search.blank?
    render :partial => "live_search" 
  end
	
  #receives file uploads from the show action and deals with them
  def upload
    loadkind(params[:id])
    file_type = (params['attachment']['image'] != nil)?'image':'documentation'
    handleupload(params['attachment'],file_type); 
    #displays error if image file upload is not in fact an image file type
    if @kind.update_attributes(params[:kind]) == FALSE
      flash['upload_notice_'+file_type] = @kind.errors.full_messages.to_s
    end
      redirect_to :back
  end
  
  #receives file uploads from the show action and deals with them
  def grab
    loadkind(params[:id])
    grabfile('image') if params['image']
    grabfile('documentation') if params['documentation']
    file_type = (params['image'] != nil)?'image':'documentation'
    #displays error if image file upload is not in fact an image file type
    if @kind.update_attributes(params[:kind]) == FALSE
      flash.now['upload_notice_'+file_type] = @kind.errors.full_messages.to_s
    end
    file_type = (params['image'] != nil)?'image':'documentation'
    render :partial=>'upload_controls', :locals => { :attachment_type => file_type }
  end

  # This bit of code allows you to pass regular old files to
  # file_column.  file_column depends on a few extra methods that the
  # CGI uploaded file class adds.  We will add the equivalent methods
  # to file objects if necessary by extending them with this module. This
  # avoids opening up the standard File class which might result in
  # naming conflicts.

  module FileCompat # :nodoc:
    attr_writer :local_path
    attr_reader :local_path
    def original_filename
      File.basename(@local_path)
    end
    def size
      length
    end
    def content_type
      nil
    end
  end
  

  def destroy_attachment
  	@attachment = Attachment.find(params[:id])
  	@kind = @attachment.kind
  	attachment_type = (@attachment.image != nil)?'image':'documentation'
  	@attachment.destroy
  	#flash['upload_notice_'+attachment_type] = attachment_type + ' attachment deleted'
    render :partial=>'upload_controls', :locals => { :attachment_type => attachment_type }
  end
  
#  # DEBUGGING METHOD
#  def delete_all
#  	for item in Item.find(:all)
#  		item.destroy
#  	end
#  	Kind.removeunused
#  	redirect_to_index('Items and types reset')
#  end
#  
#  # DEBUGGING METHOD
#  def import_data	
#	load "#{File.dirname(__FILE__)}/../../script/importdata.rb"
#  	redirect_to_index('Data imported')
#  end

  
 
  
  private
  
  #loads the items that should be in the current category, considering the
  #value of session['show_retired']
  def load_items_considering_retirement_status
    @number_of_retired_items = Item.count(["items.category_id = ? AND items.status = 'retired'", params[:id]])
    if session['show_retired'] == 'true'
      conditions = ["items.category_id = ?", params[:id]]    
    else
      conditions = ["items.category_id = ? AND (items.status != 'retired' or items.status is NULL)", params[:id]]    
    end    
    order_by = get_order_by
    if Settings.allow_pagination.to_i == 1
      per_page = session['per_page'].to_i
    else
      per_page = 10000
    end
    begin
      @item_pages, @items = paginate(:items, { :per_page => per_page, :include => [:kind], :conditions => conditions, :order => order_by })
    rescue
      @item_pages, @items = paginate(:items, { :per_page => per_page, :include => [:kind], :conditions => conditions })
      # if not calling paginate(), do this:
      #@item_pages = ''
      #@items = ''
    end
  end
  
  #safely generates an orderby SQL fragment
  def get_order_by
    session['per_page'] = 1000 if !session['per_page']
    raise "InvalidOrderString" if params['order'] and params['order'] != 'type' and !Item.content_columns.map{|col| col.name }.include?(params['order'])
    raise "InvalidDirString" if params['dir'] and !['ASC', 'DESC'].include?(params['dir'])
    params['order'] = 'type' if !params['order']
    params['dir'] = 'ASC' if !params['dir']
    real_orderby = 'items.' + params[:order]
    real_orderby = 'kinds.name' if params['order'] == 'type'
    order_by = real_orderby + " " + params['dir']
    
    #dont attempt to order by further columns if this is a search
    unless params[:text]
      found_index = @displayed_cols.index(params['order'])
      if ( found_index == nil )
        # since params['order'] is not inside @displayed_cols, we must be ordering by type name.
        # therefor the next column to order by must be @displayed_cols[0]
        next_index_in_displayed_cols = -1
      else
        #this means that params['order'] is found inside @displayed_cols
        next_index_in_displayed_cols = found_index
      end
      if @displayed_cols
        @displayed_cols.each_index do |index| 
          order_by << (', items.'+@displayed_cols[index].to_s+' '+params['dir']) if (index > next_index_in_displayed_cols)
        end
      end
    end
    order_by
  end  
  
  def handleupload(parameters,file_type)
  	if parameters[file_type] and parameters[file_type].size > 0
    	attachment = Attachment.new
    	case file_type
    		when 'image'
    	      attachment.image = parameters[file_type]
    		when 'documentation'
    		  attachment.documentation = parameters[file_type]
    		end
        #flash['upload_notice_'+file_type] = file_type + " file uploaded."    		
    	if @kind.attachments << attachment
    	else
    	  flash['upload_notice_'+file_type] = "Incorrect file type. File not uploaded."
    	end
    else
      flash['upload_notice_'+file_type] = "File not uploaded."
    end
  end
 
  #grabs files off the web
  def grabfile(file_type)
    if params[file_type] != ''
    	uri = URI.parse(params[file_type])
    	case 
    	when (uri.class == URI::HTTP || uri.class == URI::FTP)
    		require 'open-uri'
		    thefile = open(params[file_type])
			thefile.extend(FileCompat)

			thefile.local_path = params[file_type]
			params['attachment'] = Hash.new
    		params['attachment'][file_type] = thefile
    		handleupload(params['attachment'],file_type)
    	else 
    		flash['upload_notice_'+file_type] = "Protocol not supported. Could not grab file. "
    	end
    else
      flash['upload_notice_'+file_type] = "Could not grab file."
    end  
    rescue
 	   flash['upload_notice_'+file_type] = "File transmission problem. Could not grab file. "	 
  end 
  
  #loads the item with id params[:id] from the items table into the @item instance variable
  def loaditem
    @item = Item.find(params[:id])
    rescue  ActiveRecord::RecordNotFound
    	redirect_to_index("Item not found")
  end
  
  #loads the item with id params[:kind] from the kinds table into the @kind instance variable
  def loadkind(kindid)
    @kind = Kind.find(kindid)
    rescue 
    	redirect_to_index("Type not found")
  end
  
  #returns the 10 most recently modified items
  def get_last_ten
    Item.find(:all, :limit => 10, :include => ['kind'], :order => 'updated_on desc')
  end
  
  #loads @items, @default_ctd and @displayed cols (required to render the _render_view_list.rhtmlk partial)
  def load_resources_for_view_list
    load_displayed_cols
    load_items_considering_retirement_status
    load_default_ctd
    #sets @columns_to_render to the list of columns that _view_list.rhtml should render
    if session['show_all_fields'] == 'true'
      @columns_to_render = @default_ctd 
    else
      @columns_to_render = @displayed_cols
    end
  end
  
  #loads the displayed columns for this category into @displayed_cols
  def load_displayed_cols
    @displayed_cols = @current_category.displayed_fields_arr
  end
  
  #loads the hidden columns for this category into @hidden_cols
  def load_hidden_cols
    @hidden_cols = @current_category.hidden_fields_arr
  end
  
  #loads the defaults columns to display into @default_ctd
  #(this is calculated based on which columns in @items have content)
  def load_default_ctd
    @default_ctd = Category.default_colstodisplay(@items)
  end
  
  #adds any columns in @default_ctd that have not been explicitly allocated to either
  #@displayed_cols or @hidden_cols to the end of @hidden_cols
  #also cleans up @hidden_cols: if it contains any columns that are not in @default_ctd
  #then these are removed and @hidden_cols is overwritten with the fresh version
  def merge_hidden_cols_with_defaults_ctd
    #don't do anything if the deserialized @hidden_cols is corrupt
    if @hidden_cols.class == Array
      hc = @hidden_cols.clone
      hc.delete_if { |hidden_col| !@default_ctd.include?(hidden_col) }
      if hc != @hidden_cols
        @current_category.hidden_fields = YAML::dump((hc || Array.new))
        @current_category.save
        @hidden_cols = hc
      end
    
  	@default_ctd.each do |column|
  	  if !@displayed_cols.include?(column) and !@hidden_cols.include?(column)
          @hidden_cols << column
        end
      end
    end
  end
  
  #loads any columns that have not been explicitly allocated to either
  #@displayed_cols or @hidden_cols into @unclassified_cols
  def load_unclassified_cols
    @unclassified_cols = Array.new
	Item.content_columns.each do |column|
	  unless (@displayed_cols.include?(column.name) || @hidden_cols.include?(column.name))
        @unclassified_cols << column.name
      end
    end
    @unclassified_cols.sort!
  end
  
  #loads required info then renders the view_list html fragment
  def render_view_list
    load_resources_for_view_list
    render :partial => 'render_view_list'
  end
  
  
end
