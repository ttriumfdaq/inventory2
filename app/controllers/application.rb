
require_dependency "login_system"

# Filters added to this controller will be run for all controllers in the application.
# Likewise, all the methods added will be available for all controllers.
class ApplicationController < ActionController::Base

  include LoginSystem
  model :user
  
  before_filter :set_state
  
#  def start_timer
#    @start_time = Time.now
#  end
  
#  #stores the location of the current page so that if we next go to a login
#  #page then the login page will know when to redirect when you successfully login
#  #
#  #also used as a target for redirection in actions that simply perform a task then redirect 
#  #to :back because redirect_to :back would redirect to the wrong place if we intercepted the request with 
#  #a login page before allowing it to proceed
#  def store_location
#    session[:return_to] = request.request_uri unless (params[:action] == 'view_list') || (params[:controller] == 'account' and params[:action] == 'login') || params[:action] == 'rss' || params[:action] == 'delete_all'
#  end
  
  #tells log_entry model what the remote ip amnd username is in case we need to create a log entry
  #This is necessary because models cannot directly access the request  and session objects
  def set_state
    LogEntry.set_ip(request.env["REMOTE_ADDR"])
    if session and session[:user]
      LogEntry.set_username(session[:user].user_name) 
    else
      LogEntry.set_username(nil) 
    end
  end

  def redirect_to_index(message)
    flash[:notice] = message
  	redirect_to :controller => 'frontend', :action => 'index', :id => nil			
  end
  
#def rescue_action_in_public
#	flash[:notice] = "Simon's catch-all error handler has been triggered by something."
#	redirect_to(:action => 'index')
#end


  #redirects to the last page we were on, not including login.  This is important because it will
  #redirect correctly when a logged out user attempts to perform a restricted action which redirects to 
  #back, but instead has the login page inserted between the original request and the action itself.
  def redirect_to_back
    redirect_to @session[:return_to]
  end
  
  #a component that renders the menu
  def menu
    render(:layout => false) 
  end
   


end