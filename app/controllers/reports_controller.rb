class ReportsController < ApplicationController
  
  before_filter :login_required, :only => [:delete_all]
  
  SCRIPTS_DIRECTORY = RAILS_ROOT + '/script/report_generators/'
  
  def index

    @scripts = Filesystem::get_files(SCRIPTS_DIRECTORY, ['.rb'])
    @reports_generated = Settings.reports_generated
  end
  
  # This bit of code allows you to pass regular old files to
  # file_column.  file_column depends on a few extra methods that the
  # CGI uploaded file class adds.  We will add the equivalent methods
  # to file objects if necessary by extending them with this module. This
  # avoids opening up the standard File class which might result in
  # naming conflicts.

#  module FileCompat # :nodoc:
#
#    #This removes the .pid.n that is appended to the filename by Tempfile
#    def original_filename
#      temp = File.basename(local_path).to_s
#      while temp =~ /\d\Z|\.\Z/ 
#        temp.chop! 
#      end
#      temp 
#      
#    end
#    
#    def local_path
#      path
#    end
#    
#    def content_type
#      nil
#    end
#  end


  
  #this function generates a report in HTML PDF and/or CSV formats based on a script stores in 
  #the /script/reportgenerators directory
  def generate
  
  require RAILS_ROOT + '/lib/ruport.rb'
  
  
    if params['commit']
      if Filesystem.file_exists?(SCRIPTS_DIRECTORY, ['.rb'], params['script'])
        require 'ruport'
        load SCRIPTS_DIRECTORY + params['script']
        start_time = Time.now
        @data_set = CustomReport::create_data_set
        #only attempt to generate file formats that are possible to generate
        possible_types = ['csv','html','simple_table_pdf']
        
        #types = Array.new
        #params['format'].each_key {|key| types << key if possible_types.include?(key) }
        #print out the table in the requested formats
        #types.each do |type|
          extension = (type == 'simple_table_pdf')?'pdf':params['commit']
          filename = File.basename(params['script'], '.rb') + '.' + extension  
          require 'timeout'
          status = Timeout::timeout(300) {
          output = @data_set.as(params['commit'].to_sym) do |builder| 
            builder.header = "TRIUMF Inventory System"
            builder.output_type = :complete
          end
          #File.open(filename, 'w') { |file| file.write output }
            #thefile = Tempfile.new(filename)
            Settings.reports_generated = Settings.reports_generated.to_i+1
            send_data output, :filename => 'report.' + extension
#			thefile.extend(FileCompat)
#            attachment = Attachment.new
#            attachment.report = thefile
#    		attachment.save
          }
        #end
        #flash[:notice] = 'Report generation time: ' + (Time.now - start_time).to_s + ' seconds.'
        
      else
#        flash[:notice] = "Script not found."
      end
    else
#      flash[:notice] = "No output formats specified."
    end
    
    #redirect_to :action => :view_reports
  
  end
  
  #streams a report file from a location on the server filesystem
  #back to the client.  The point of this is that it makes it
  #easy to restrict access to backup files to logged-in users.
#  def stream_report_file
#    send_file(Report.stream_report_file(params[:id]))
#  end
  
  #a component that renders the list of existing reports
#  def view_reports
#    @number_of_reports = Attachment.count('report IS NOT NULL')
#    @report_pages, @reports = paginate(:attachments, :order_by => 'id DESC', :conditions => 'report IS NOT NULL')
#    render :layout => false
#  end
  
  
  #removes all currently generated reports from the disk and the sql database
#  def delete_all
#    for attachment in Attachment.find(:all, :conditions => "report IS NOT NULL")
#      attachment.destroy
#    end
#    redirect_to_back
#  end
#  
#
#

end
