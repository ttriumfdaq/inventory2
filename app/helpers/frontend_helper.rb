module FrontendHelper

  #generates an ajax snippet that updates the po link (used in show view)
  def update_show(do_what)
    remote_function(:url => {:controller => 'frontend', :action => 'update_show', :id => params[:id], :do => do_what})
  end
  

  #displays the up or down (direction) arrow 
  #that appears in the heading columns in list views
  def dirswitch(fieldname)
    action = 'list'
    action = 'search' if params['text']
  	if params[:order] == fieldname
  	  if params['dir'] == 'DESC'
  		link_to("&uarr; "+fieldname.humanize, :action => action, :id => params[:id], :order => fieldname, :dir => 'ASC', :page => params['page'], 'text' => params['text'])
  	  else
        link_to("&darr; "+fieldname.humanize, :action => action, :id => params[:id], :order => fieldname, :dir => 'DESC', :page => params['page'], 'text' => params['text'])
  	  end
  	else
   		link_to(fieldname.humanize, :action => action, :id => params[:id], :order => fieldname, :dir => 'ASC', :page => params['page'], 'text' => params['text'])
  	end
  end
	

  #returns a nicely formatted image display containing all the images associated with an object
  def thumbnailbox(object)
  	size = '160x120'
  	output = '<div class="imagewrap">'
  	object.getimages.each do |image|
  		output += thumblink(image,size)
  		output += '<br />' if size == '160x120'
  		size = '39x30'
  	end
  	output += '</div>'
  end
	
  #returns each image (accompagnied by a deletion link if the user is logged in)
  def images_edit_list(object)
  	output = String.new
  	i = 1
  	object.getimages.each do |attachment|
  	  i = 1 - i.abs
      output += "<div class=\"imagewrap\">\n"
      begin
        output +=  thumblink(attachment,'160x120')
  	  rescue
  	    output += "Error: image in database but not on filesystem."
  	  end
  	  output +=  "<br />" + destroy_button('destroy_attachment', attachment, 'upload_image') if session[:user]
  	  output += "\n</div>\n"
  	  output += "<div style=\"clear:both;\"></div>\n" if i == 1
  	end
  	output
  end
	
  #returns the documentation files associated with a particular object (a Kind instance)
  #formatted in an unordered list
  def doclist(object, deletelink = nil)
  	output = "<ul>\n"
  	object.getdocs.each do |attachment|
      begin
        url = url_for_file_column(attachment,'documentation')
        link = link_to(File.basename(url), url, :popup => ['new_window'])
        sizetext = ' (' + human_size(File.size(attachment.documentation)) + ')'
  	  rescue Errno::ENOENT
  	    link = "Error: file in database but not on filesystem."
  	  end
  		
  		delete = destroy_button('destroy_attachment', attachment, 'upload_documentation') if deletelink
  		
  		output += '<li>' + link.to_s + sizetext.to_s + delete.to_s + "</li>\n"
  		
  	end
  	output += '</ul>'
  end
  
  #generates the tiny thumbnails appearing in the list action
  #def tinythumblink(kind)
  #  attachment = kind.getimage
  #  thumblink(attachment, '40x40') if attachment
  #end
	
  private 
  
  #returns a thumbnail wrapped in a link to the full image
  def thumblink(attachment,size)
  	link_to(image_tag(url_for_image_column(attachment, 'image', :size => size)),url_for_image_column(attachment, 'image'),:popup => ['new_window'])
  end

  #returns the top level category of where we currently are 	
#  def gettoplevelcat
#  	case
#  	when params[:id]
#  		toplevelcat = Item.find(params[:id].to_i).category.parent.id
#  	when params[:cat]
#  		toplevelcat = Category.find(params[:cat].to_i).parent.id
#  	else
#  		toplevelcat = 2
#  	end			
#  end
  
  #prints a dropdown menu allowing us to select the category (used on both the new action and the show action)
  def category_dropdown(onchange_submit=false)
    collection_select_with_current(:item, :category, Category.find_all_except_root, :id, :full_name_output, @item.category.id, onchange_submit)
  end
  
  #an ajax submit button for the list view (re-used several times!)
  def submit_to_remote_batch(button_text,task,confirm_text)
    submit_to_remote('task', button_text, {:loading => "Form.disable('ids_selection_form');", :url => { :action => :batch_change, :id => params['id'], :task => task, :order => params['order'], :dir => params['dir'], :page => params[:page] }, :confirm => "Are you sure you want to " + confirm_text + "?"})
  end
  
  


end
