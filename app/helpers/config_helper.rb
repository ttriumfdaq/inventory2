module ConfigHelper

  def category_edit_form(action)
    #by default the category we belong to is root (essential for
    #when we are adding a new item)
    @category = Category.new unless @category
    form_remote_tag({:url => {:action => 'affect_category_ajax', :ajax_action => action, :id => @category.id}, :update => {:success => 'greybox', :failure => "#{action}_error"}, :before => "Element.update('new_error','');Element.update('edit_error','')", :complete =>  remote_function(:update => 'rendered_tree', :url => {:controller => 'application', :action => 'menu'})+';'+visual_effect(:Appear, "error_hider") }) + 'name: ' + text_field('category', 'name', {:size => 10}) + ' parent: ' + collection_select_with_current(:category, :parent_id, Category.find_all, :id, :full_name_output, @category.parent_id) + submit_tag("save") + ' ' + end_form_tag
  end
  

end
