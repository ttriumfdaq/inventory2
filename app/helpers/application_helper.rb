# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  
  #build an array with each entry being an element in the title
  def title_array
    arr = Array.new #format of sub-arrays inserted into arr is [text,controller,action,id]
    case
    when (params['controller'] == 'frontend' and params[:action] == 'index')
      arr << ['home','frontend',nil,nil]
    when params['controller'] == 'config'
      arr << ['configuration','config','index',nil]
      arr << [params[:action],'config',params[:action],nil]
    when (params['controller'] == 'account' and params[:action] == 'config')
      arr << ['configuration','config','index',nil]
      arr << ['users','account','config',nil]
    when (params['controller'] == 'account' and params[:action] == 'login' and params['root_required'] == 'true')
      arr << ['login',nil,nil,nil]
      arr << ['acquire root privileges',nil,nil,nil]
    when (params['controller'] == 'account' and params[:action] == 'login')
      arr << ['login',nil,nil,nil]
    when (params['controller'] == 'account' and params[:action] == 'change_password')
      arr << ['change password',nil,nil,nil]
    when params['controller'] == 'reports'
      arr << ['reports','reports','index',nil]
    when params['controller'] == 'help'
      arr << ['help','help','index',nil]
    when (params['controller'] == 'frontend' and params[:action] == 'show')
      @item.category.full_name.each{|category| arr << [category.name, 'frontend', 'list', category.id]}
      arr << [@item.kind.name,'frontend','show',params[:id]]
    when params[:action] == 'list'
      Category.find(params[:id]).full_name.each{|category| arr << [category.name, 'frontend', 'list', category.id]}
    when params[:action] == 'search'
      arr << ["search for \"#{params[:text]}\"",nil,nil,nil]
    when (params[:action] == 'new' || params[:action] == 'create')
      arr << ['new item',nil,nil,nil]
    when params[:action] == 'support'
      arr << ['feedback','frontend','support',nil]
    else
      arr << [params[:controller],params[:controller],'index',nil]
      arr << [params[:action],params[:controller],params[:action],params[:id]]
    end
    arr
  end
  
  
  #generated the breadcrumbs from the results of a call to title_array
  def generate_title(arr, links = false, separator = " &#187; ")
    output = String.new
    arr.each_index do |i| 
      output << separator unless i == 0
      if arr[i][1] == nil and arr[i][2] == nil and arr[i][3] == nil
        if links
          output << link_to( h(arr[i][0].humanize), @request.env['REQUEST_URI'])
        else
          output << h(arr[i][0].humanize)
        end
      else
        if links
          output << link_to( h(arr[i][0].humanize), :controller => arr[i][1], :action => arr[i][2], :id => arr[i][3] )
        else
          output << h(arr[i][0].humanize)
        end
        
      end
    end
    output
  end
  
  
  #generates the breadcrumb leading to an item. this is 
  #used by the data fields editor in the config controller
  def item_breadcrumb(item)
    link_path(item.category.full_name) + " &#187; " + link_to(item.kind.name, {:controller => 'frontend', :action => :show, :id => item.id})
  end
  
  #prints out a destroy button
  #if update is set to the name of a div, then an ajax button is displayed instead
  #note that this function is slow when repeated many times because it has to look up hashes to figure
  #out the route
  def destroy_button(action,id, update=false)
    if update
      link_to_remote(image_tag('destroy.png'), { :update => update, :url => { :action => action, :id => id }, :confirm => 'Are you sure?' } ) 
    else
      link_to image_tag('destroy.png'), { :action => action, :id => id}, :post => true, :confirm => 'Are you sure?' 
    end
    
  end
  
  #returns the full category listing as nested HTML unordered lists
  #The code block &renderer is a renderer for each individual category 
  #which is passed id_of_cat, name_of_cat, in_top_level, has_children, ancestors_of_open_category_ids
  #HASH ARGUMENTS: 
  #:open_category - if not set then display every category in the db, else
  #only display siblings of :open_category and their direct children
  #

  def category_iterator(root = Category.root, options = {}, &renderer)
    #fetch the ancestors of the current item unless they have already been fetched or we do
    #not need them due to the fact that options[:open_category] is unspecified (all categories open)
    unless options[:ancestors_of_open_category_ids] || options[:open_category] == nil
      options[:ancestors_of_open_category_ids] = Array.new
      options[:ancestors_of_open_category_ids] = Category.find(options[:open_category]).ancestors.collect!{|x| x[:id]}.push(options[:open_category]) 
#   STDERR.puts "DEBUG"
#   STDERR.puts options[:ancestors_of_open_category_ids]
    end
    options[:in_top_level] = true if options[:in_top_level] == nil
    if root.children.size > 0
      outputstring = "\n<ul>\n"
      for child in root.children( :order => 'parent_id ASC')
        outputstring += "<li>"
        crawl_further_into_child = true
        #if :open_category is specified then manipulate crawl_further_into_child
        #so that we only display the correct subcategories
        if options[:open_category]
          crawl_further_into_child = false
          crawl_further_into_child = true if options[:ancestors_of_open_category_ids].include?(child.id)
        end
        outputstring += renderer.call(child.id, child.name, options[:in_top_level], !child.children.empty?, options[:ancestors_of_open_category_ids]).to_s
        options2 = options.dup
        options2[:in_top_level] = false
        outputstring += (category_iterator(child, options2, &renderer)).to_s if crawl_further_into_child
        outputstring += "</li>\n"
      end
      outputstring += "</ul>\n"
    end
  end
  
  #formats full_name output from a Category object nicely with links and separators
  def link_path(full_name)
    full_name.collect!{|x| link_to(x.name.humanize, {:controller => 'frontend', :action => :list, :id => x.id})}.join(" &#187; ")
  end
 
 #this is a patch because collection_select as implemented by Rails does 
 #not select the current item.
  def collection_select_with_current(object, method, collection, value_method, text_method, current_value, onchange_submit = false)
    oc = String.new
    oc =  ' onchange="this.form.submit();"' if onchange_submit == true
    result = "<select name='#{object}[#{method}]'#{oc}>\n" 
    for element in collection
      if current_value.to_i == element.send(value_method).to_i
        result << "<option value='#{element.send(value_method)}' selected>#{element.send(text_method)}</option>\n" 
      else
        result << "<option value='#{element.send(value_method)}'>#{element.send(text_method)}</option>\n" 
      end
    end
    result << "</select>\n" 
    return result
  end
  
  
  #convenience method that creates a dropdown menu in a consistent format
  def dropdown(name, options, current_value, display_submit_tag = true, display_form_tag = true, submit_text = 'update',onchange_submit=false)
    if @item.nil?
      id = nil
    else
      id = @item.id
    end
    stag = ''
    stag = submit_tag(submit_text, :id => 'submit') if display_submit_tag
    sformtag = String.new
    eformtag = String.new
    if display_form_tag 
      sformtag = form_tag({:action => 'change_'+name, :id => id})
      eformtag = end_form_tag
    end
    if onchange_submit
      ocs = 'this.form.submit();'
    else
      ocs = nil
    end
    sformtag + select_tag(name,options_for_select(options.collect!{ |x| x.to_s },current_value.to_s),{:onchange => ocs}) + stag + eformtag 
  end
  
  #returns a standard link unless we are at the same action and id, in which case it returns a colored link
  def link_to_unless_current_action_and_id(name, options = {}, html_options = {})  
    id_copy = params['current_id']
    action_copy = params['current_action']
    options[:controller] = 'frontend' if options[:controller] == nil
    options[:action] = 'index' if options[:action] == nil
    if (params['current_controller'] == 'frontend' and params['current_action'] == 'show')
      action_copy = 'list'
      id_copy = Item.find(params['current_id']).category.id.to_s
    end    
    if (params['current_controller'] == options[:controller] and action_copy == options[:action]  and id_copy.to_s == options[:id].to_s) || (params['current_controller'] == 'config' and options[:controller] == 'config') || (params['current_controller'] == 'help' and options[:controller] == 'help')
      html_options[:class] = 'selected'
      link_to(name, options, html_options) 
    else
      link_to(name, options, html_options) 
    end
  end
  
  #returns a standard link or a colored link
  def link_to_unless_current_controller_and_action(name, options = {})  
    html_options = Hash.new
    if (params[:controller] == options[:controller] and params[:action] == options[:action]) || (params[:controller] == options[:controller] and options[:controller] == 'config' ) || (params[:controller] == 'account' and params[:action] == 'config' and options[:controller] == 'config' )
      html_options[:class] = 'selected'
      link_to(name, options, html_options) 
    else
      link_to(name, options, html_options) 
    end
  end
  

  
end
