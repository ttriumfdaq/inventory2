module ReportsHelper


  # Identical to +pagination_links+ except uses +link_to_remote+ instead of
  # +link_to+.  Additional options are:
  # +:update+::   same as in +link_to_remote+
  def pagination_links_remote(paginator, options={})
    update = options.delete(:update)

    str = pagination_links(paginator, options)
    if str != nil
      str.gsub(/href="([^"]*)"/) do
        url = $1
        "href=\"#\" onclick=\"new Ajax.Updater('" + update + "', '" + url +
        "', {asynchronous:true, evalScripts:true}); return false;\"" 
      end
    end
  end

end

