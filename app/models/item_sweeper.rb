#NOTE: this class is coupled to the method save_preferences in FrontendController
#because save_preferences performs a manual fragment expiration based on the file
#format defined in this class

class ItemSweeper < ActionController::Caching::Sweeper
  observe Item
  
  def before_save(item)
    item_cleanup(item)
  end
  
  def before_destroy(item)
    item_(item)
  end
  
  #calculates all the list views that an item affects, then expires them all
  def item_cleanup(item)
    STDERR.puts "EXPIRING STUFF FOR #{item.kind.name}"
    expire_fragment(/showtoken_#{item.id}_showtoken/)
    ancestors = item.category.ancestors.collect{|cat| cat.id}.reverse
    ancestors << item.category.id
    ancestors.each{|ancestor| expire_fragment(/vltoken_#{ancestor}_vltoken/)}
    STDERR.puts "ANCESTORS DEALT WITH: "
    ancestors.each {|an| STDERR.puts an }
  end
  
end