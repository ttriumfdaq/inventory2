class Note < ActiveRecord::Base
  belongs_to :item
  belongs_to :user
  
  validates_presence_of :message
  
  def after_save 
    LogEntry.create('note saved ' + id.to_s)
  end
    
  def after_destroy
    LogEntry.create('note deleted ' + id.to_s)
  end 
end
