require_dependency "search" 
	
	
	
class Item < ActiveRecord::Base


  belongs_to :kind # these are correct (see p.221)!
  belongs_to :category
  has_many :notes, :exclusively_dependent => true
  has_many :changes, :exclusively_dependent => true
  validates_associated :kind, :message => " name can't be blank"
  validates_associated :category
  after_save :cleantypes
  after_destroy :cleantypes
  
  def after_create 
    LogEntry.create('item created ' + id.to_s)
  end

  def after_update
    LogEntry.create('item updated ' + id.to_s)
  end
  
  def after_destroy
    LogEntry.create('item deleted ' + id.to_s)
  end
  
	
#	
#  #customized new method that correctly sets up the foreign keys to the 
#  #kind and the category
#  def self.newitem(params)
#    #STDERR.puts "\n\n\n ITEM:\n "+ params['item'].inspect
#  	item = self.new
#  	columns = Item.content_columns.collect{|col| col.name }
#  	allcolsaregood = true
#  	params['item'].keys.each do |key| 
#  	  if columns.include?(key)
#  	    item.send(key) = params['item'][key]
#  	  else
#  	    allcolsaregood = ("\n"+key+"\n") 
#  	  end
#  	end
#  	STDERR.puts "\n\n\n\n\n\n\n\n ERROR: COLUMNS DO NOT MATCH UP WITH AVAILABLE FIELDS IN THE ITEMS TABLE!!!\n#{allcolsaregood} \n\n\n\n\n\n\n\n"if allcolsaregood != true
#  	params['item'].keys.each{|key| item.update()
#  end

  #sets the kind_id foreign key. if the kind does not currently
  #exist it adds a new entry to the kinds table else it links up to the
  #existing kind in the kinds table	
  def self.setkind(kind_name,item)
  	item.kind = Kind.find(:first, :conditions => ['name = ?', kind_name])
  	if item.kind == nil
  		item.kind = Kind.new({"name" => kind_name}) 
  	end
  end
	
  #sets the category_id foreign key field on an item
  def self.setcategory(category_id,item)
    item.category = Category.find(category_id)
  end
  
  
  def cleantypes
  	#Kind.removeunused
  end
  
  #ensures that any blank fields are set to nil so that we are not storing blank
  #strings in the database (the nils get turned into NULLs automatically)
  def before_save
    attribute_names.each {|name| send(name.to_s + '=', nil) if (send(name) == '' and name != 'created_on' and name != 'updated_on')}
  end
  
  #redefines content_columns to just return the text labels, rather than column objects
  #note that we handle the case where content_columns returns this already as well as
  #the case where it returns column objects, because content_columns has unpredictable
  #results (sometimes returns on or the other for no apparent reason)
  def self.content_columns_text
    cc = content_columns
    output = cc
    if cc != nil
      if cc[0].instance_of?(String)
        output = cc
      else
        output = content_columns.map{|column| column.name}
      end
    end
    output
  end
  
  
  #same as Item.content_columns except it also gets rid of created_on and updated_on
  def self.content_columns_without_stamps
    content_columns_text.delete_if{ |column| column == "created_on" || column == "updated_on" }
  end
  
  #used in list view for batch field updates
  def self.content_columns_without_stamps_or_status
    content_columns_text.delete_if{ |column| column == "created_on" || column == "updated_on" || column == 'status' }
  end
 
  #fetches the status options out of YAML and dumps them into an array
  #prepend_blank is used in the show view so that we can always set status to ' '
  def self.load_status_options(prepend_blank = false)
    status_options = YAML::load(Settings.status_options)
    status_options = Array.new unless status_options
    status_options = status_options.insert(0,' ') if prepend_blank == true
    status_options
  end
  
  #changes the kind to a new kind. also creates a Change and a LogEntry.
  def change_kind(new_kind, user_id)
    old_value = self.kind.name
    self.kind = new_kind
    save
    user = User.find(user_id)
    Change.create( :item => self, :user => user, :field => 'type', :old_value => old_value, :new_value => self.kind.name )
  end
   
end
