
class Category < ActiveRecord::Base
	has_many :items
	validates_presence_of :parent_id, :name
	acts_as_tree :order => "parent_id ASC, name ASC"
	attr_accessible :name, :parent_id
	validates_length_of :name, :maximum=> 20

	
  def after_save 
    LogEntry.create('category saved ' + id.to_s)
  end
  
  def after_destroy
    LogEntry.create('category deleted ' + id.to_s)
  end
  

  #prevents us from destroying categories that are not empty
  def before_destroy
    unless is_empty? 
      errors.add_to_base("'#{name}' is not empty")
      raise StandardError, 'category not empty' 
    end
  end
  
  #returns true if this category does not contain any items or subcategory
  def is_empty?
     items.count == 0 and children.count == 0
  end

  #if this category is already a toplevel cat, return category.
  #else return the top-level category under which category is located.
  def self.belongs_to_which_toplevel(category)
   if is_top_level?(category)
     return category 
    else
     return category.parent
    end
  end
	
  #recurses up to the root from the current category, returning 
  #the full path to the category (not including root itself)
  def full_name
    self.ancestors.reverse!.push(self).delete_if{|x| !x.parent_id }
  end
  
  #formats the array from full_name nicely for use in forms
  def full_name_output
    full_name.collect!{|x| x.name.humanize}.join(" &#187; ")
  end
  
  # returns an array with all categories below this category
  def get_subcategories(root = self)
    arr = Array.new
    arr << root unless root == self
    for child in root.children
      arr.concat get_subcategories(child)
    end
    arr
  end
  
  #returns all categories except root
  def self.find_all_except_root
    self.find_all.delete_if{|x| !x.parent_id }
  end
  
  #redefines a built in function to order properly (used in config)
  def self.find_all
    find(:all, :order => 'parent_id ASC, name ASC')
  end
  
  #loads hidden fields from YAML into an array
  def hidden_fields_arr
    arr = YAML::load(hidden_fields)
    #if the YAML is corrupt then reset to a blank array
    (arr.class == Array)?arr:Array.new
  end
  
  #loads displayed fields from YAML into an array
  def displayed_fields_arr
    arr = YAML::load(displayed_fields)
    #if the YAML is corrupt then reset to a blank array
    (arr.class == Array)?arr:Array.new
  end
  
  #returns colstodisplay according to how it would look in a default situation, when the
  #user has not set any preferences (i.e. only display columns that have 1 or more items
  #with a value in that column in the current category)
  def self.default_colstodisplay(items)
    colstodisplay = []
    for column in Item.content_columns_text
      current_column_is_empty = TRUE
      for item in items
        current_column_is_empty = FALSE if item.send(column)
      end
      colstodisplay << column unless current_column_is_empty
    end		
    return colstodisplay         
  end
  
  

end
