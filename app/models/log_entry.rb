class LogEntry < ActiveRecord::Base

  def self.set_ip(ip)
    @@ip = ip
  end
  
  def self.set_username(username)
    @@username = username
  end


  #method that logs events to the inventory_system_log
  #log will rotate when it reaches 10 megabytes
  def self.create(text)
    entry = LogEntry.new( :created_on => Time.now, :user => @@username.to_s, :ip => @@ip.to_s, :text => text)
    entry.save
  end


end