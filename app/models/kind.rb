require_dependency "search" 
	
	
	
class Kind < ActiveRecord::Base

	has_many :attachments, :dependent => TRUE
	has_many :items # these are correct (confirm on p.219)
	validates_presence_of :name
	validates_uniqueness_of :name
	validates_length_of :name, :minimum => 1
	
  
  def after_create 
    LogEntry.create('kind created ' + id.to_s)
  end

  def after_update
    LogEntry.create('kind updated ' + id.to_s)
  end

  def after_destroy
    LogEntry.create('kind deleted ' + id.to_s)
  end 

	#returns an array with all the images that are associated with a kind
	def getimages
		attachments.find(:all, :conditions => 'image is not NULL')
	end
	
	#returns an array with all the documentation files associated with a kind
	def getdocs
		attachments.find(:all, :conditions => 'documentation is not NULL')
	end
	
	#destroys kinds that have no associated items
	def self.removeunused
		for kind in Kind.find(:all)
			if kind.items.empty?
				STDERR.puts "removing kind with no associated items: " + kind.name				
			  kind.destroy
			end
		end
	end
	
  #redefines content_columns to just return the text labels, rather than column objects
  #note that we handle the case where content_columns returns this already as well as
  #the case where it returns column objects, because content_columns has unpredictable
  #results (sometimes returns on or the other for no apparent reason)
  def self.content_columns_text
    cc = content_columns
    output = cc
    if cc != nil
      if cc[0].instance_of?(String)
        output = cc
      else
        output = content_columns.map{|column| column.name}
      end
    end
    output
  end
  
  
  #same as Item.content_columns except it also gets rid of created_on and updated_on
  def self.content_columns_without_stamps
    content_columns_text.delete_if{ |column| column == "created_on" || column == "updated_on" }
  end
  
  
  
  
  #ensures that any blank fields are set to nil so that we are not storing blank
  #strings in the database (the nils get turned into NULLs automatically)
  def before_save
    attribute_names.each {|name| send(name.to_s + '=', nil) if (send(name) == '' and name != 'created_on' and name != 'updated_on')}
  end
    
  
  
	
end
