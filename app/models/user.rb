# this model expects a certain database layout and its based on the name/login pattern. 
class User < ActiveRecord::Base
  
  @@validate_old_password = true
  attr_accessor :old_password
  
  #this prevents a user record from being updated unless the virtual attribute old_password is correct 
  #or the current user is logged in as root
  validates_each :old_password do |model, attr, value|
    #if id is present then obviously the user must already exist and therefor 
    #we must be performing an update, not creating a new record
    unless model.id == nil || !@@validate_old_password
      model.errors.add(attr, 'is incorrect') unless User.hash_password(value) == model.hashed_password
    end
  end
  

  
  
  attr_accessor :password, :has_root
  attr_accessible :user_name, :password, :password_confirmation
  
  #when this method is called, the system will no longer validate the old password field
  #this is the exception to the rule, and is only called by the root user from config -> users
  def self.disable_old_password_validation
    @@validate_old_password = false
  end
  
  def after_save 
    password = nil #for security
    LogEntry.create('user saved ' + user_name)
  end
  
  def after_destroy
    LogEntry.create('user deleted ' + user_name)
  end 
  
  def before_save 
    self.hashed_password = User.hash_password(password)
  end
  
  def before_destroy
    raise "It is not possible to delete the root user." if self.user_name == 'root'
  end

  # Authenticate a user. 
  def try_to_login
    User.login(self.user_name, self.password)
  end
  
  def self.login(user_name, password)
    hashed_password = User.hash_password(password || "")
    find_by_user_name_and_hashed_password(user_name, hashed_password)
  end
  
  def self.root_password_correct?(password)
    root_user = find_by_user_name('root')
    root_user.hashed_password == User.hash_password(password)
  end

  protected

  def self.hash_password(password)
    Digest::SHA1.hexdigest(password)
  end
  
  validates_uniqueness_of :user_name, :on => :create

  validates_length_of :user_name, :within => 3..40
  validates_length_of :password, :within => 5..40
  validates_presence_of :user_name, :password
  validates_confirmation_of :password
  
end
