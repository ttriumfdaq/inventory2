class KindSweeper < ActionController::Caching::Sweeper
  observe Kind
  
  def before_save(kind)
    cleanup(kind)
  end
  
  def before_destroy(kind)
    cleanup(kind)
  end
  
  #calculates all the items that a kind affects, then tells each one to clean up after itself
  def cleanup(kind)
    kind.items.each{|item| item_cleanup(item) }
  end
  
  #calculates all the list views that an item affects, then expires them all
  def item_cleanup(item)
    STDERR.puts "EXPIRING STUFF FOR #{item.kind.name}"
    expire_fragment(/showtoken_#{item.id}_showtoken/)
    ancestors = item.category.ancestors.collect{|cat| cat.id}.reverse
    ancestors << item.category.id
    ancestors.each{|ancestor| expire_fragment(/vltoken_#{ancestor}_vltoken/)}
  end
  
end