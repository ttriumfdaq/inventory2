



#contains functions to create and retrieve backups
class Backup


  #convenience method that returns the backup directory
  def self.backups_dir
    Settings.backup_storage_location + '/'
  end
  require('zip/zip')
  require('zip/zipfilesystem')
  require ('filesystem')
  

  ATTACHMENT_DIR = File.dirname(__FILE__) + '/../../public/attachment'
  


  #dumps out the database and copies everything in the attachments directory into a 
  #zip file named according to the current date & time in iso 8601 format
  #returns the name of the file created
  def self.new
    load_database_access_info
    #work out the filename for the backup zip file, and only continue if this file
    #is not already present
    filename = backups_dir + Time.now.iso8601 + '.zip'
    if File.exist?(filename)
      raise 'File writing error because file already exists. Backup cancelled.'
    else
      #dump the database and backup the files into the zip file
      command = "mysqldump -c -l -u #{@@username} --password=\"#{@@password}\" #{@@db}"
  	  outputfile = String.new
  	  
  	  Zip::ZipFile.open(filename, Zip::ZipFile::CREATE) do |zipfile|
        zipfile.file.open("database.sql", "w") { |f| f.write(IO.popen(command, "r").readlines.join) }
        Dir.chdir(ATTACHMENT_DIR)
        files =  Dir.glob("**/*")
        files.each { |f| zipfile.add(f,f) }
        #files.each { |f| STDERR.puts "DEBUGGING: " + f.to_s }
        outputfile = zipfile
	  end
	  LogEntry.create('backup created ' + outputfile.to_s)
	  outputfile.to_s
	end
  end
  
  #sends a backup file to the client (only if they're logged in and the file exists)
  def self.stream_backup_file(filename)
    if Filesystem.get_files(backups_dir, ['.zip']).include?(filename)
      LogEntry.create('backup downloaded ' + filename)
      return (backups_dir + filename)
    else
      raise "Backup file not found."
    end
  end

  def self.restore(file)
    if Filesystem.get_files(backups_dir, ['.zip']).include?(file) 
      filename = backups_dir+file.to_s
      LogEntry.create('backup restored ' + filename.to_s)
      #load the database from the zip file onto the mysql server
      load_database_access_info
      command = "mysql -u #{@@username} --password=\"#{@@password}\" #{@@db}"
  	  Zip::ZipFile.open(filename, "r") do |zipfile|
        IO.popen(command, "w") do |io|
          io.write zipfile.file.read("database.sql")
        end
	  end
      #delete all the files and directories in the attachment directory
      FileUtils.rm_r(ATTACHMENT_DIR) if File.directory?(ATTACHMENT_DIR)
      FileUtils.mkdir(ATTACHMENT_DIR)
      Dir.chdir(ATTACHMENT_DIR)
	  #recreates the directory structure
	  Zip::ZipFile.foreach(filename) do |file| 
        FileUtils.mkdir_p(file.to_s) if file.directory?
      end
      #put the attachment files back into the directory structure
	  Zip::ZipFile.open(filename) do |zipfile|
  	  Zip::ZipFile.foreach(filename) do |entry| 
          if entry.file?
            File.new(entry.to_s, 'w').write(zipfile.file.read(entry.to_s))  
          end
        end
      end
    else
      raise "Backup file not found."
    end 
  end
 

 
 private
 
  #loads the rails database access info so that we can directly call mysqldump
  #and mysql client when backing up / restoring
  def self.load_database_access_info
    #grab the database access information from activerrecord
    @@db = ActiveRecord::Base.configurations['production']['database']
    @@username = ActiveRecord::Base.configurations['production']['username']
    @@password = ActiveRecord::Base.configurations['production']['password']
  end
  

 
end
