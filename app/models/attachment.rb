class Attachment < ActiveRecord::Base
	belongs_to :kind
	file_column :image
	file_column :documentation
	file_column :report, :fix_file_extensions => nil
	validates_file_format_of :image, :in => ["gif", "png", "jpg"], :message => " are invalid. Did you try to upload an image that was not a JPEG, PNG or GIF?"
    
    def after_save 
      LogEntry.create('attachment saved ' + id.to_s)
    end
    
    def after_destroy
      LogEntry.create('attachment deleted ' + id.to_s)
    end
    
    private
    
    #examines the image documentation and report fields, returning the contents of the one that is not empty 
    def get_filename
      return image if image
      return documentation if documentation
      return report if report
    end
end
