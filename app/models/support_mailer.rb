class SupportMailer < ActionMailer::Base

  #sends a support request / bug report from a user 
  def request(ip, name, email, message, sent_at = Time.now)
    @subject    = '[Inventory system support request]'
    @recipients = [Settings.email_address]
    @cc = [Settings.email_cc]
    @from       = email
    @sent_on    = sent_at
    @headers    = {}
    @body["name"] = name
    @body["email"] = email
    @body["ip"] = ip
    @body['message'] = message
  end
  


  #sends an error report whenever an error occurs
  def error(sent_at = Time.now)
    @subject    = 'SupportMailer#error'
    @body       = {}
    @recipients = 'sclaret@gmail.com'
    @from       = ''
    @sent_on    = sent_at
    @headers    = {}
  end
end
