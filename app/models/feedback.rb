# model for email form input 
class Feedback < ActiveForm

  attr_accessor :name, :email, :message
  
  validates_presence_of :name, :message
  
  validates_length_of :email, :in => 5..200

  validates_length_of :name, :maximum => 100
  
  validates_length_of :message, :maximum => 100000

end