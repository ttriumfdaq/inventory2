
class CategorySweeper < ActionController::Caching::Sweeper
  observe Category
  
  #expires all cached fragments
  def before_save(category)
    expire_fragment(/./)
  end
  
  #expires all cached fragments
  def before_destroy(category)
    expire_fragment(/./)
  end
  
end