module ActiveRecord
  module VERSION #:nodoc:
    MAJOR = 1
    MINOR = 14
    TINY  = 1

    STRING = [MAJOR, MINOR, TINY].join('.')
  end
end
